<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email_to_send');
            $table->string('date_from')->nullable();
            $table->string('date_to')->nullable();
            $table->string('age_from')->nullable();
            $table->string('age_to')->nullable();
            $table->string('modalities')->nullable();
            $table->string('Procedures')->nullable();
            $table->string('gender')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polls');
    }
}
