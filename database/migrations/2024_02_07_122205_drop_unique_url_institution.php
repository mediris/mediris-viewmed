<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUniqueUrlInstitution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        $this->down();
        
        Schema::table('institutions', function (Blueprint $table) {

            $table->dropUnique(['url']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::table('institutions', function (Blueprint $table) {
           
            $table->unique(['url']);
        
        });
    }
}
