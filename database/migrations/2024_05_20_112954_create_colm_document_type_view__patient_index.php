<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmDocumentTypeViewPatientIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public function up(){

    $this->down();
    $this->createView();
}
    public function createView()
    {
        DB::statement("
            CREATE VIEW `patientsIndexView` AS
            SELECT 
                `p`.`id` AS `id`,
                `p`.`active` AS `active`,
                (CASE
                    WHEN `p`.`document_type` IS NULL OR `p`.`document_type` = '' THEN `p`.`patient_ID`
                    ELSE CONCAT(`p`.`document_type`, '-' , `p`.`patient_ID`)
                END) AS `patientID`,
                `p`.`last_name` AS `lastName`,
                `p`.`first_name` AS `firstName`,
                `p`.`email` AS `email`,
                `p`.`telephone_number` AS `phoneNumber`,
                `p`.`cellphone_number` AS `cellPhoneNumber`,
                `p`.`birth_date` AS `birthDate`,
                `p`.`photo` AS `photo`,
                TIMESTAMPDIFF(YEAR, `p`.`birth_date`, NOW()) AS `age`
            FROM
                `patients` `p`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS patientsIndexView');
    }
}
