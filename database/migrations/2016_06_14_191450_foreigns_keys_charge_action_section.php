<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysChargeActionSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_action_section', function($table){
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('restrict');
            $table->foreign('action_section_id')->references('id')->on('action_section')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_action_section', function(Blueprint $table){
            $table->dropForeign('role_action_section_role_id_foreign');
            $table->dropForeign('role_action_section_action_section_id_foreign');
        });
    }
}
