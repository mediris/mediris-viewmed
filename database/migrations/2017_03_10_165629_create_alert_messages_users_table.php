<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertMessagesUsersTable extends Migration{

    public function up()
    {
        Schema::create('alert_messages_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('institution_id')->unsigned();
            $table->integer('alert_message_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('alert_messages_users');
    }
}
