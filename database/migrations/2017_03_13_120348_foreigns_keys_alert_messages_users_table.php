<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysAlertMessagesUsersTable extends Migration
{

    public function up(){
        Schema::table('alert_messages_users', function(Blueprint $table){
            $table->foreign('institution_id')->references('id')->on('institutions')->onDelete('restrict');
            $table->foreign('alert_message_id')->references('id')->on('alert_messages')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
        });
    }


    public function down(){
        Schema::table('alert_messages_users', function(Blueprint $table){
            $table->dropForeign('alert_messages_users_institution_id_foreign');
            $table->dropForeign('alert_messages_users_alert_message_id_foreign');
            $table->dropForeign('alert_messages_users_user_id_foreign');
        });
    }
}
