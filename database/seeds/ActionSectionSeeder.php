<?php

use Illuminate\Database\Seeder;
use App\Action;
use App\Section;

class ActionSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($action_section = null, $sections = null){

        if(is_null($action_section)){
            // Must be Loaded
            $sections = Section::all();
            $actions  = Action::all();

            foreach ($sections as $section){
                foreach ($actions as $index => $action) {
                    Action::find($index + 1)->sections()->attach($section);
                }
                // Old wear method
                /*Action::find(1)->sections()->attach($section);
                Action::find(2)->sections()->attach($section);
                Action::find(3)->sections()->attach($section);
                Action::find(4)->sections()->attach($section);
                Action::find(5)->sections()->attach($section);
                Action::find(6)->sections()->attach($section);
                Action::find(7)->sections()->attach($section);
                Action::find(8)->sections()->attach($section);
                Action::find(9)->sections()->attach($section);
                Action::find(10)->sections()->attach($section);
                Action::find(11)->sections()->attach($section);
                Action::find(12)->sections()->attach($section);
                Action::find(13)->sections()->attach($section);
                Action::find(14)->sections()->attach($section);*/
            }
        } else {
            $sections = [];
            foreach ($action_section as $key => $value) {
                if($value->section->id > 0 && $value->action->id > 0){
                    //var_dump(' Section ID: ' . $value->section->id . ' ' . $value->section->value['value'] . ' Action ID: ' . $value->action->id);
                    $exist = Action::find($value->action->id)->sections()->where('section_id', '=', $value->section->id)->exists();
                    if(!$exist){
                       Action::find($value->action->id)->sections()->attach(Section::find($value->section->id));
                    }
                    // Section
                    $section = array(
                        'id'    => $value->section->id,
                        'value' => $value->section->value['value'],
                    );
                    if(!in_array($section['id'], array_column($sections, 'id'))){ $sections[] = $section; }
                }
            }

            if(isset($sections)){
                foreach ($sections as $section) {
                    $exist = Action::find(1)->sections()->where('section_id', '=', $section['id'])->exists();
                    if(!$exist){
                       Action::find(1)->sections()->attach(Section::find($section['id']));
                    }
                }
            }
        }

    }
}
