<?php

use Illuminate\Database\Seeder;
use App\Sex;

class SexesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sexes = ['male', 'female'];

        foreach($sexes as $sex)
        {
            Sex::create([
                'name' => $sex,
                'administrative_ID' => strtoupper(substr($sex, 0, 1))
            ]);
        }
    }
}