<?php

use Illuminate\Database\Seeder;
use App\Prefix;

class PrefixesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Commom titles
         */

        Prefix::create([
            'name' => 'Master.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Master.',
            'language' => 'es',
        ]);

        Prefix::create([
            'name' => 'Mr.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Sr.',
            'language' => 'es',
        ]);

        Prefix::create([
            'name' => 'Miss.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Srta.',
            'language' => 'es',
        ]);

        Prefix::create([
            'name' => 'Ms.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Sra.',
            'language' => 'es',
        ]);

        Prefix::create([
            'name' => 'Mrs.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Mx.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Maid.',
            'language' => 'en',
        ]);


        /*
         * Formal titles
         */

        Prefix::create([
            'name' => 'Sir.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Sir.',
            'language' => 'es',
        ]);

        Prefix::create([
            'name' => 'Madam.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Madam.',
            'language' => 'es',
        ]);

        Prefix::create([
            'name' => 'Dame.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Lord.',
            'language' => 'en',
        ]);


        Prefix::create([
            'name' => 'Lady.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Dama.',
            'language' => 'en',
        ]);


        /*
         * Dr/Professor Titles
         */

        Prefix::create([
            'name' => 'Dr.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Prof.',
            'language' => 'en',
        ]);

        Prefix::create([
            'name' => 'Dr.',
            'language' => 'es',
        ]);

        Prefix::create([
            'name' => 'Prof.',
            'language' => 'es',
        ]);



    }
}
