<?php

use Illuminate\Database\Seeder;
use App\Icon;

class IconsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $icons = [
            'f2b9',
            'f2ba',
            'f2bb',
            'f2bc',
            'f042',
            'f2a3',
            'f13d',
            'f187',
            'f1fe',
            'f047',
            'f07e',
            'f07d',
            'f2a2',
            'f069',
            'f1fa',
            'f29e',
            'f1b9',
            'f24e',
            'f05e',
            'f19c',
            'f080',
            'f02a',
            'f0c9',
            'f2cd',
            'f240',
            'f244',
            'f243',
            'f242',
            'f241',
            'f236',
            'f0fc',
            'f0f3',
            'f0a2',
            'f1f6',
            'f1f7',
            'f206',
            'f1e5',
            'f1fd',
            'f29d',
            'f0e7',
            'f1e2',
            'f02d',
            'f02e',
            'f097',
            'f2a1',
            'f0b1',
            'f188',
            'f1ad',
            'f0f7',
            'f0a1',
            'f140',
            'f207',
            'f1ba',
            'f1ec',
            'f073',
            'f274',
            'f272',
            'f133',
            'f271',
            'f273',
            'f030',
            'f083', //camera retro
            'f1b9',
            'f20a',
            'f0a3', //fa-certificate
            'f236',
            'f2c9',
            'f0ce',
            'f0f3',
            'f016',
            'f21e',
            'f08a',
            'f1ae',
            'f059',
            'f055',
            'f255',
            'f007',
            'f2bd',
            'f135'
        ];

        foreach($icons as $icon)
        {
            Icon::create([
                'description' => '&#x'.$icon.';',
            ]);
        }
    }
}
