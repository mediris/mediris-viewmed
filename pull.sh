#!/bin/bash

echo Script para cerrar las sesiones de los usuarios activos en el sistema.

sleep 2s

cd --

cd /var/www/html/mediris

php artisan kill:sessions

echo Sesiones cerradas exitosamente.

sleep 2s

echo Deteniendo servicio de apache.....


systemctl stop httpd

echo Servicio de apache detenido.
