<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alerts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the alerts triggers on the app.
    |
    */
    'done-orders' => 'REPORTE DE ÓRDENES REALIZADAS',
    'dictated-orders' => 'REPORTE DE ÓRDENES DICTADAS',
    'approved-orders' => 'REPORTE DE ÓRDENES APROBADAS',
    'transcribed-orders' => 'REPORTE DE ÓRDENES TRANSCRITAS',
    'orders-list' => 'LISTADO DE ÓRDENES',
    'appointments' => 'REPORTE DE CITAS',
    'radiologist-fees' => 'HONORARIOS DE RADIÓLOGO',
    'technician-fees' => 'HONORARIOS DE TÉCNICO',
    'transcriber-fees' => 'HONORARIOS DE TRANSCRIPTOR',
    'nulled-requisitions' => 'HONORARIOS DE requisiciones anuladas',
    'birads' => 'REPORTE DE BIRADS',
    'patients-by-genre' => 'PACIENTES POR GÉNERO',
    'orders-to-dictate' => 'ÓRDENES PENDIENTES POR DICTAR',
    'orders-to-approve' => 'ÓRDENES PENDIENTES POR APROBAR',
    'orders-to-transcribe' => 'ÓRDENES PENDIENTES POR TRANSCRIBIR',
    'eficiency-indicator' => 'INDICADOR DE EFICIENCIA',
    'transcription' => 'REPORTE DE TRANSCRIPCIÓN',


];