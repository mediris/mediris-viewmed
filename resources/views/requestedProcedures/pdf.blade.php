<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ trans('labels.order-info') }}</title>
    <meta http-equiv="Content-Type" content="titleext/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('/css/pdfs.css') }}">
</head>
<body id="app-layout" class="{{ $draft }}">
    <script type="text/php">
        $size = 8;
        $y = 10;
        $x = $pdf->get_width() - 85;
        $font = $fontMetrics->get_font("sans-serif");
        $pdf->page_text($x, $y, trans('labels.page') . " {PAGE_NUM} " . trans('labels.of') . " {PAGE_COUNT}", $font, $size);
    </script>

    <div class="header">
        @if($header)
            <img src="{{ $header }}" class="header-img">
        @endif
        <table>
            <tr>
                <td class="label">{{ strtoupper( trans( 'labels.date' ) ) }}:</td>
                <td>{{ $date }}</td>
                <td class="label">{{ strtoupper( trans( 'labels.type' ) ) }}:</td>
                <td>{{ $type }}</td>
                <td class="label">{{ strtoupper( trans( 'labels.report' ) ) }}:</td>
                <td>{{ $id }}</td>
            </tr>
            <tr>
                <td class="label">{{ strtoupper( trans( 'titles.patient' ) ) }}:</td>
                <td colspan="3">{{ $name }}</td>
                <td class="label">C.I.:</td>
                <td>{{ $ci }}</td>
            </tr>
            <tr>
                <td class="label">{{ strtoupper( trans( 'labels.birth-date-small' ) ) }}:</td>
                <td colspan="3">{{ $birthdate }}</td>
                <td class="label">{{ strtoupper( trans( 'labels.sex' ) ) }}:</td>
                <td>{{ $sex }}</td>
            </tr>
            <tr>
                @if(is_null($referring))
                    <td class="label">{{ strtoupper( trans( 'labels.referring' ) ) }}:</td>
                    <td colspan="5">{{ strtoupper( trans( 'labels.not-specified' ) ) }}</td>
                @else
                    <td class="label">{{ strtoupper( trans( 'labels.referring' ) ) }}:</td>
                    <td colspan="5">{{ $referring }}</td>
                @endif
            </tr>
            <tr>
                <td class="label">{{ strtoupper( trans( 'labels.study' ) ) }}:</td>
                <td colspan="5">{{ $study }}</td>
            </tr>
        </table>
    </div>

    <div class="footer">
        <div class="signature">
            <img src="{{ $signature }}" class="firma" >
            <div class="radiologist" >{{ $signatureRadiologist }} - {{ $signatureRadiologistCharge }}</div>
                @if(isset($radiologistCredentialsMpps))
                    <div class="credeniales">C.I. {{$radiologistCredentialsCi}} / {{$radiologistCredentialsMpps}} / {{$radiologistCredentialsCdmc}}</div>
                @endif
            <div class="date">{{ $signatureDate }}</div>
        </div>
        @if($footer)
            <p>{{str_random(40)}}</p> 
            <img src="{{ $footer }}" class="footer-img">
        @endif
    </div>

    <div class="content">
        {!! $text !!}
    </div>
</body>
</html>

<style>
    @if($signature)
        .footer {
            bottom: 4.5cm;
        }
        body {
            margin-bottom: 3cm;
        }
    @else
        .footer {
            bottom: 2cm;
        }
        body {
            margin-bottom: 1.5cm;
        }
    @endif

    .footer p{

        text-align: right; 
        color: #C2C2C2;
    }

    .label{
        font-size: 13px;
    }

    td{
        font-size: 13px;
 
    }

    .content{

        font-size: 13px;
        text-align: justify!important;

    }

    .header-img{
        max-height: 120px;
    }
    .header{
        margin-bottom: 1px!important;    
    }
    body{

        margin-top: 250px!important;
        margin-bottom: 220px!important;
        font-size: 13px;
    }

    .footer{

        margin-top: 0px!important;
        margin-bottom: 100px!important;
    }
    .firma{

        width: 400px; 
        min-height: 80px;
    }

</style>