@extends('layouts.app')

@section('title', ucfirst(trans('titles.medical-visitors')))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.medical-visitors')),
									'elem_type' => 'a',
									'elem_name' => ucfirst(trans('labels.add')),
									'form_id' => '',
									'route' => route('medicalvisitors.add'),
									'fancybox' => 'add-row cboxElement',
									'routeBack' => '',
									'clearFilters' => true
								])
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							<li>{{ Session::get('message') }}</li>
						</ul>
					</div>
				@endif
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				
				<div class="title">
					<h1>{{ ucfirst(trans('titles.medical-visitors')) }}</h1>
				</div>
				
				<?php

				// Grid
                $options = (object) array();
                $kendo    = new \App\CustomKendoGrid($options, false, false);

                // Fields
                $IdField = new \Kendo\Data\DataSourceSchemaModelField('id');
				$IdField->type('string');
				$FirstNameField = new \Kendo\Data\DataSourceSchemaModelField('first_name');
				$FirstNameField->type('string');
				$LastNameField = new \Kendo\Data\DataSourceSchemaModelField('last_name');
				$LastNameField->type('string');
				$EmailField = new \Kendo\Data\DataSourceSchemaModelField('email');
				$EmailField->type('string');
				$TelField = new \Kendo\Data\DataSourceSchemaModelField('telephone_number');
				$TelField->type('string');
				$CellField = new \Kendo\Data\DataSourceSchemaModelField('cellphone_number');
				$CellField->type('string');

				$kendo->addFields(array(
					$IdField,
					$FirstNameField,
					$LastNameField,
					$EmailField,
					$TelField,
					$CellField,
				));

                // Create Schema
                $kendo->createSchema(false, false);

                // Create Data Source
                // $kendo->createDataSource();

                $visitors = $visitors->toArray();
				
				// $canShow = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'referrings', 'show');
				// $canEdit = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'referrings', 'edit');
				// $canEnable = Auth::user()->hasAccess(Session::get('institution')->id, Session::get('roles')[0]->id, 'referrings', 'active');
				
				foreach ($visitors as $key => $value) {
					$visitors[$key]['email'] = '<a href=mailto:' . $visitors[$key]['email'] . '>' . $visitors[$key]['email'] . '</a>';
					$visitors[$key]['telephone_number'] = html_entity_decode('<a href=tel:' . $visitors[$key]['telephone_number'] . '>' . $visitors[$key]['telephone_number'] . '</a>');
					$visitors[$key]['cellphone_number'] = html_entity_decode('<a href=tel:' . $visitors[$key]['cellphone_number'] . '>' . $visitors[$key]['cellphone_number'] . '</a>');
					$title = $visitors[$key]['active'] == 1 ? trans('labels.disable') : trans('labels.enable');
					$active = $visitors[$key]['active'] == 0 ? "fa fa-check" : "dashboard-icon icon-deshabilitar";
					
					//$showAction = $canShow ? '<a href=' . route('medicalvisitors.show', [$visitors[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.show') . '><i class="fa fa-eye" aria-hidden="true"></i></a>' : '';
					$showAction = '<a href=' . route('medicalvisitors.show', [$visitors[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.show') . '><i class="fa fa-eye" aria-hidden="true"></i></a>';
					
					// $editAction = $canEdit ? '<a href=' . route('medicalvisitors.edit', [$visitors[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement"><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>' : '';
					$editAction = '<a href=' . route('medicalvisitors.edit', [$visitors[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . trans('labels.edit') . ' class="edit-row cboxElement"><i class ="dashboard-icon icon-edit" aria-hidden=true></i></a>';
					
					// $activeAction = $canEnable ? '<span class="slash">|</span> <a href= ' . route('medicalvisitors.active', [$visitors[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>' : '';
					$activeAction = '<span class="slash">|</span> <a href= ' . route('medicalvisitors.active', [$visitors[$key]['id']]) . ' data-toggle="tooltip" data-placement="bottom" title=' . $title . '><i class ="' . $active . '" aria-hidden=true></i></a>';
					
					$visitors[$key]['actions'] = $editAction . $activeAction;
				}

				// Set  Visitors
				$kendo->setDataSource($visitors);

				// Create Grid
                $kendo->createGrid('visitors');

                // Columns
                $Id = new \Kendo\UI\GridColumn();
				$Id->field('id')
						->filterable(false)
						->title('Id');
				$FirstName = new \Kendo\UI\GridColumn();
				$FirstName->field('first_name')
						->title(ucfirst(trans('labels.name')));
				$LastName = new \Kendo\UI\GridColumn();
				$LastName->field('last_name')
						->title(ucfirst(trans('labels.last-name')));
				$Email = new \Kendo\UI\GridColumn();
				$Email->field('email')
						->encoded(false)
						->title(ucfirst(trans('labels.email')));
				$Tel = new \Kendo\UI\GridColumn();
				$Tel->field('telephone_number')
						->encoded(false)
						->title(ucfirst(trans('labels.telephone-number')));
				$Cell = new \Kendo\UI\GridColumn();
				$Cell->field('cellphone_number')
						->encoded(false)
						->title(ucfirst(trans('labels.cellphone-number')));
				$Actions = new \Kendo\UI\GridColumn();
				$Actions->field('actions')
						->sortable(false)
						->filterable(false)
						->encoded(false)
						->title(ucfirst(trans('labels.actions')));

				 // Filter
                $kendo->setFilter();

                // Pager
                $kendo->setPager();

                // Column Menu
                $kendo->setColumnMenu();

                $kendo->addcolumns(array(
					$FirstName,
					$LastName,
					$Email,
					$Tel,
					$Cell,
					$Actions,
                ));
				
                // Defines & Generate
                $kendo->generate();

                // Redefines Data Bound
                $kendo->dataBound('onDataBound');

				?>
				
				{!! $kendo->render() !!}
			
			</div>
		</div>
	</div>

@endsection