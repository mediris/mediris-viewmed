@extends('layouts.app')

@section('title',ucfirst(trans('titles.print-label')))

@section('content')

	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.print-label')),
									'elem_type' => 'button',
									'elem_id' => 'print-label',
									'elem_name' => ucfirst(trans('labels.print')),
									'form_id' => '',
									'route' => '',
									'fancybox' => '',
									'routeBack' => route('technician')
								])

	<div class="container-fluid technician printer printable hidden-print">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="panel sombra">
					<div class="order-label">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<img class="img-responsive" src="{{ Session::get('institution')->logo }}" alt="">
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="order-label-body">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="order-label-heading name">
													<h3 class="uppercase" >{{ $patient->last_name }} {{ $patient->first_name }}</h3>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
												<p>CI: {{ $patient->patient_ID }}</p>
											</div>
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
												<p>
													{{ ucfirst(trans('labels.age')) }}
													: {{ $patient_ageinfo['years'] . ' '  . ($patient_ageinfo['years'] == 1 ?  trans('labels.year') : trans('labels.years')) }}
													{{ ', ' . $patient_ageinfo['months'] . ' ' . ($patient_ageinfo['months'] == 1 ? trans('labels.month') : trans('labels.months')) }}
												</p>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="order-label-heading procedure-name">
													<h4 class="uppercase">{{ $requestedProcedure->procedure->description }}</h4>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="order-label-heading patient-type">
													<h4 class="uppercase">{{ $requestedProcedure->service_request->patient_type->description }}</h4>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
												<p>{{ trans('labels.accession-number') . ': ' . $requestedProcedure->id }}</p>
												{{--<p>{{ ucwords($requestedProcedure->procedure->rooms->name) }}</p>--}}
											</div>
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
												<p>{{ trans('labels.service-request-id') . ': ' . $requestedProcedure->service_request->id }}</p>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												<p>{{ trans('labels.date') . ': ' . $requestedProcedure->technician_end_date }}</p>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												<p style="text-align: left !important;">
													@if ( $referring )
														{{trans('labels.referring') . ':'}}
														<strong>
															{{ ucwords($referring->first_name) . " " . ucwords($referring->last_name) }}
														</strong>
													@endif
												</p>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												<p class="institution_id no-bold text-center" style="text-align: center !important; margin: 15px auto -20px auto;">
													{{ Session::get('institution')->name }}
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
		.uppercase {
			text-transform: uppercase;
		}
		@media print{
			body, #app-layout {
				margin: 0 !important;
			}
			img {
				max-height: 50px !important;
			}
			h2 {
				font-size: 12px !important;
			}
			.sidebar-large #wrapper {
				min-height: 0;
			}
			.bold {
				font-weight: bold !important;
			}
			.no-bold {
				font-weight: normal !important;
			}
			.no-margin {
				margin: 0 !important;
			}
			.font-110 {
				font-size: 110% !important;
			}
			.font-120 {
				font-size: 120% !important;
			}
			.text-center {
				text-align: center !important;
			}
			.text-overflow {
				display: block;
				display: -webkit-box;
				max-width: 100%;
				margin: 2.5px 0 !important;
				line-height: 1;
				-webkit-line-clamp: 2;
				-webkit-box-orient: vertical;
				overflow: hidden;
				text-overflow: ellipsis;
			}
			.text-overflow.regular {
				height: 30px !important;
				font-size: 15px !important;
			}
			.text-overflow.small {
				height: 24px !important;
				font-size: 12px !important;
			}
			.negative-margin-15 {
				margin-top: -15px !important;
			}
			.flex__container {
				display: flex;
				justify-content: flex-start;
				align-items: flex-start;
				flex-direction: column;
			}
			.flex__container > * {
				flex: 1;
				display: flex;
				justify-content: space-between;
				align-items: space-between;
			}
			.flex__container > * > * {
				margin: 2.5px;
			}
			.flex__container  > .logo {
				align-self: flex-start !important;
				margin: -10px auto 5px auto !important;
			}
			.flex__container > .logo > img {
				max-height: 90px;
			}
			.flex__container .institution {
				width: 100%;
				text-align: center;
			}
		}
	</style>
	<!-- Section to print -->
	<div class="container-fluid visible-print-block order-label avoid-page-break-after flex__container">
		<div class="logo">
			<img class="img-responsive no-margin" src="{{ Session::get('institution')->logo }}" alt="">
		</div>
		<div class="no-margin">
			<h1 class="bold no-margin text-overflow regular uppercase">
				{{ $patient->last_name }} {{ $patient->first_name }}
			</h1>
		</div>
		<div class="no-margin">
			<p>
				CI: {{ $patient->patient_ID }}
			</p>
			<p>
				{{ ucfirst(trans('labels.age')) }}
				: {{ $patient_ageinfo['years'] . ' '  . ($patient_ageinfo['years'] == 1 ?  trans('labels.year') : trans('labels.years')) }}
				{{ ', ' . $patient_ageinfo['months'] . ' ' . ($patient_ageinfo['months'] == 1 ? trans('labels.month') : trans('labels.months')) }}
			</p>
		</div>
		<div class="{{ Session::get('institution')->logo ? 'no-margin' : '' }} uppercase">
			<h2 class="bold font-110 text-overflow regular">
				{{ $requestedProcedure->procedure->description }}
			</h2>
		</div>
		<div class="{{ Session::get('institution')->logo ? 'no-margin' : '' }} uppercase">
			<h2 class="bold text-overflow small">
				{{ $requestedProcedure->service_request->patient_type->description }}
			</h2>
		</div>
		<div class="{{ Session::get('institution')->logo ? 'no-margin' : '' }}">
			<p class="small {{ Session::get('institution')->logo ? 'no-margin' : '' }}">
				{{ trans('labels.accession-number') . ': ' . $requestedProcedure->id }}
			</p>
			<p class="small {{ Session::get('institution')->logo ? 'no-margin' : '' }}">
				{{ trans('labels.service-request-id') . ': ' . $requestedProcedure->service_request->id }}
			</p>
		</div>
		<div class="{{ Session::get('institution')->logo ? 'no-margin' : '' }}">
			<p class="small {{ Session::get('institution')->logo ? 'no-margin' : '' }}">
				{{ trans('labels.date') . ': ' . $requestedProcedure->technician_end_date }}
			</p>
		</div>
		<div class="{{ Session::get('institution')->logo ? 'no-margin' : '' }}">
			<p class="small {{ Session::get('institution')->logo ? 'no-margin' : '' }}">
				@if ( $referring )
					{{trans('labels.referring') . ':'}}
					<span class="bold">
						{{ ucwords($referring->first_name) . " " . ucwords($referring->last_name) }}
					</span>
				@endif
			</p>
		</div>
		<div class="{{ Session::get('institution')->logo ? 'no-margin' : '' }}">
			<p class="no-bold {{ Session::get('institution')->logo ? 'no-margin' : '' }} institution">
				{{ Session::get('institution')->name }}
			</p>
		</div>
	</div>
	<!-- End print -->
@endsection
