<div class="container-fluid">

    <form method="post" action="{{ route('suspend_reasons.add') }}" id="SuspendReasonAddForm">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="modal-header">
                <h4 class="modal-title">{{ ucfirst(trans('titles.add')).' '.trans('titles.suspendreasons') }}</h4>
            </div>
        </div>

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
                </div>
                <div>
                    <input type="text" name="description" id="description"
                           class="input-field form-control user btn-style" value="{{ old('description') }}"/>
                    @if ($errors->has('description'))
                        <span class="text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include('includes.general-checkbox', [
                    'id'        =>'admin_reason-chk',
                    'name'      =>'admin_reason',
                    'label'     =>'labels.admin-reason',
                    'condition' => 0
                ])
                @if ($errors->has('admin_reason'))
                    <span class="text-danger">
                        <strong>{{ $errors->first('active') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="modal-footer">
                <button class="btn btn-form" id="btn-add-edit-save" data-style="expand-left" type="submit">
                    <span class="ladda-label">{{ ucfirst(trans('labels.save')) }}</span>
                </button>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\SuspendReasonAddRequest', '#SuspendReasonAddForm'); !!}
</div>
<script>
    //ColorBoxSelectsInit();
    ColorBoxmultiSelectsInit();
</script>


    