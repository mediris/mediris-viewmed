@extends('layouts.app')

@section('title',ucfirst(trans('titles.add')).' '.trans('titles.user'))

@section('content')

	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.users')),
									'elem_type' => 'button',
									'elem_name' => ucfirst(trans('labels.save')),
									'form_id' => '#form-1',
									'route' => '',
									'fancybox' => '',
									'routeBack' => route('users')
								])

	<div class="container-fluid">

		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
			</div>
		</div>

		<form id="form-1" method="post" action="{{ route('users.add') }}" enctype="multipart/form-data"
		      id="UserAddForm">
			{!! csrf_field() !!}

			<div class="row">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<!-- CITAS -->
					<div class="panel sombra x6 elastic" id="dates-notification-panel">
						<div class="panel-heading">
							<h3 class="panel-title">{{ trans('labels.main-info') }}</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									@include('includes.general-checkbox', [
									    'id'        =>'active-chk',
									    'name'      =>'active',
									    'label'     =>'labels.is-active',
									    'condition' => 0
									])								
									@if ($errors->has('active'))
										<span class="help-block"> {{ $errors->first('active') }}</span>
									@endif
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }}
											*</label>
									</div>
									<div>
										<input type="text" class="form-control" name="administrative_ID"
										       id="administrative_ID"
										       value="{{ old('administrative_ID') }}">
										@if ($errors->has('administrative_ID'))
											<span class="help-block">
                                            {{ $errors->first('administrative_ID') }}
                                        </span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for="position">{{ ucfirst(trans('labels.position')) }}</label>
									</div>
									<div>
										<input type="text" class="form-control" name="position" id="position" value="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='first_name'>{{ ucfirst(trans('labels.name')) }} *</label>
									</div>
									<div>
										<input type="text" class="form-control" name="first_name" id="first_name"
										       value="{{ old('first_name') }}">
										@if ($errors->has('first_name'))
											<span class="help-block">{{ $errors->first('first_name') }}</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='last_name'>{{ ucfirst(trans('labels.last-name')) }} *</label>
									</div>
									<div>
										<input type="text" class="form-control" name="last_name" id="last_name"
										       value="{{ old('last_name') }}">
										@if ($errors->has('last_name'))
											<span class="help-block">{{ $errors->first('last_name') }}</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='last_name'>M.P.P.S.</label>
									</div>
									<div>
										<input type="text" class="form-control" name="mpps" id="mpps"
										       value="{{ old('mpps') }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='last_name'>C.D.M.C.</label>
									</div>
									<div>
										<input type="text" class="form-control" name="cdmc" id="cdmc"
										       value="{{ old('cdmc') }}">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='email'>{{ ucfirst(trans('labels.email')) }} *</label>
									</div>
									<div>
										<input type="email" class="form-control" name="email" id="email"
										       value="{{ old('email') }}">
										@if ($errors->has('email'))
											<span class="help-block">{{ $errors->first('email') }}</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='username'>{{ ucfirst(trans('labels.user-name')) }} *</label>
									</div>
									<div>
										<input type="text" class="form-control" name="username" id="username"
										       value="{{ old('username') }}">
										@if ($errors->has('username'))
											<span class="help-block">{{ $errors->first('username') }}</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='password'>{{ ucfirst(trans('labels.password')) }} *</label>
									</div>
									<div>
										<input type="password" class="form-control" name="password" id="password">
										@if ($errors->has('password'))
											<span class="help-block">{{ $errors->first('password') }}</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='password_confirmation'>{{ ucfirst(trans('labels.password-confirmation')) }}
											*</label>
									</div>
									<div>
										<input type="password" class="form-control" name="password_confirmation"
										       id="password_confirmation">
										@if ($errors->has('password_confirmation'))
											<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='username_pacs'>{{ ucfirst(trans('labels.user-name-pacs')) }} </label>
									</div>
									<div>
										<input type="text" class="form-control" name="username_pacs" id="username_pacs"
											   value="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='password_pacs'>{{ ucfirst(trans('labels.password-pacs')) }} </label>
									</div>
									<div>
										<input type="password" class="form-control" name="password_pacs" id="password_pacs"
											   value="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='signature'>{{ ucfirst(trans('labels.signature')) }}</label>
									</div>
									<div class="signature-img">
										<img src="" class="img-responsive" id="signature-image">
										<input type="hidden" id="img-crop-input-signatura" value="">
										<a type="button" name="edit-signature" id="edit-signature" class="btn btn-form do-crop" target="signature"
										   title="{{ trans('labels.edit-signature') }}">
											<i class="fa fa-upload" aria-hidden="true"></i>
											{{ trans('labels.edit-signature') }}
										</a>
									</div>
									<input type="hidden" name="signature" id="signature"
									       title="Patient avatar" value="">

									@if ($errors->has('signature'))
										<span class="help-block">{{ $errors->first('signature') }}</span>
									@endif
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									
								</div>
							</div>
							<div class="row">
								<div class="panel-heading">
									<h3 class="panel-title">{{ trans('labels.roles') }}</h3>
								</div>
							</div>
							@foreach($institutions as $institution)
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<label for="roles">{{ ucfirst(trans('labels.institution')) }}</label>
										</div>
										<div>
											<input type="text" class="form-control" name="institution" id="institution[{{$institution->id}}]" value="{{ $institution->name }}" readonly>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<label for="roles">{{ ucfirst(trans('labels.roles')) }}</label>
										</div>
										<div>
											<select class="form-control" name="roles[]" id="roles">
												<option value="0">{{ ucfirst(trans('labels.none')) }}</option>
												@foreach($roles as $role)
													<option value="{{ $role->id }}">{{ $role->name }}</option>
												@endforeach
											</select>

											@if ($errors->has('roles'))
												<span class="help-block">{{ $errors->first('roles') }}</span>
											@endif
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="panel sombra x6 elastic" id="dates-notification-panel">
						<div class="panel-heading">
							<h3 class="panel-title">{{ trans('labels.general-info') }}</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for="prefix_id">{{ ucfirst(trans('labels.prefix')) }} *</label>
									</div>
									<div>
										<select class="form-control" name="prefix_id" id="prefix_id">
											<option value="">{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($prefixes as $prefix)
												<option value="{{ $prefix->id }}">{{ $prefix->name }}</option>
											@endforeach
										</select>

										@if ($errors->has('prefix_id'))
											<span class="help-block">{{ $errors->first('prefix_id') }}</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for="suffix_id">{{ ucfirst(trans('labels.suffix')) }} *</label>
									</div>
									<div>
										<select class="form-control" name="suffix_id" id="suffix_id">
											<option value="">{{ ucfirst(trans('labels.select')) }}</option>
											@foreach($suffixes as $suffix)
												<option value="{{ $suffix->id }}">{{ $suffix->name }}</option>
											@endforeach
										</select>
										@if ($errors->has('suffix_id'))
											<span class="help-block">{{ $errors->first('suffix_id') }}</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='telephone_number'>{{ ucfirst(trans('labels.telephone-number')) }}
											*</label>
									</div>
									<div>
										<input type="tel" class="form-control" name="telephone_number"
										       id="telephone_number"
										       value="{{ old('telephone_number') }}">
										@if ($errors->has('telephone_number'))
											<span class="help-block">{{ $errors->first('telephone_number') }}</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='telephone_number_2'>{{ ucfirst(trans('labels.telephone-number-2')) }}</label>
									</div>
									<div>
										<input type="tel" class="form-control" id="telephone_number_2"
										       name="telephone_number_2"
										       value="{{ old('telephone_number_2') }}">
										@if ($errors->has('telephone_number_2'))
											<span class="help-block">{{ $errors->first('telephone_number_2') }}</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='cellphone_number'>{{ ucfirst(trans('labels.cellphone-number')) }}
											*</label>
									</div>
									<div>
										<input type="tel" class="form-control" name="cellphone_number"
										       id="cellphone_number"
										       value="{{ old('cellphone_number') }}">
										@if ($errors->has('cellphone_number'))
											<span class="help-block">{{ $errors->first('cellphone_number') }}</span>
										@endif
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div>
										<label for='cellphone_number_2'>{{ ucfirst(trans('labels.cellphone-number-2')) }}</label>
									</div>
									<div>
										<input type="tel" class="form-control" name="cellphone_number_2"
										       id="cellphone_number_2"
										       value="{{ old('cellphone_number_2') }}">
										@if ($errors->has('cellphone_number_2'))
											<span class="help-block">{{ $errors->first('cellphone_number_2') }}</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='additional_information'>{{ ucfirst(trans('labels.additional-information')) }}</label>
									</div>
									<div>
                                                <textarea class="form-control" name="additional_information" rows="14"
                                                          id="additional_information">{{ old('additional_information') }}</textarea>
										@if ($errors->has('additional_information'))
											<span class="help-block">{{ $errors->first('additional_information') }}</span>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
		{!! JsValidator::formRequest('App\Http\Requests\UserAddRequest', '#form-1'); !!}
	</div>
@include('includes.img-modal-signature')
@endsection
