@extends('layouts.app')

@section('title',ucfirst(trans('titles.approve')).' '.trans('titles.order').' - '.ucfirst(trans('titles.radiologist')))

@section('content')

    @include('partials.actionbar',[ 'title' => ucfirst(trans('titles.radiologist').' - '. ucfirst(trans('titles.orders-to-approve')).' - '.trans('titles.detail')),
    'elem_type' => 'button',
    'elem_name' => ucfirst(trans('labels.approve-order')),
    'form_id' => '#RadiologistApproveForm',
    'route' => '',
    'fancybox' => '',
    'routeBack' => route('radiologist'),
    'second_elem_name' =>  ucfirst(trans('labels.reject-transcription')),
    'second_elem_id' => 'submit-reject-transcription'
])

    <div class="container-fluid radiologist edit">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-file" aria-hidden="true"></i> {{ trans('titles.patient-info') }}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="patient-img ">
                                        @if( isset($requestedProcedure->patient->id) && !empty($requestedProcedure->patient->photo))
                                            <img src="{{ asset( 'storage/'. $requestedProcedure->patient->photo ) }}"
                                                 alt="Patient-Image" class="img-responsive" id='avatar'>
                                        @else
                                            <img src="/images/patients/default_avatar.jpeg" alt="Patient-Image"
                                                 class="img-responsive" id='avatar'>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="patient-info">
                                                <p><strong>{{ ucfirst(trans('labels.name')) }}
                                                        :</strong> {{ $requestedProcedure->patient->first_name . ' ' . $requestedProcedure->patient->last_name }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.age')) }}
                                                    :</strong> {{ $requestedProcedure->patient->getStrAgeAndMonth() }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.gender')) }}
                                                    :</strong> {{ $requestedProcedure->patient->sex_id == 1 ? trans('labels.male') : trans('labels.female') }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.height')) }}:</strong> {{ $requestedProcedure->service_request->height
                                        . ' ' . trans('labels.meters') }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p><strong>{{ ucfirst(trans('labels.weight')) }}:</strong> {{ $requestedProcedure->service_request->weight
                                        . ' ' . trans('labels.kilograms') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.birth-date')) }}
                                            :</strong> {{ App::getLocale() == 'es' ? date('d-m-Y', strtotime($requestedProcedure->patient->birth_date)) : date('Y-m-d', strtotime($requestedProcedure->patient->birth_date)) }}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.history-id')) }}
                                            :</strong> {{ $requestedProcedure->patient->history_id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.address')) }}
                                            :</strong> {{ $requestedProcedure->patient->address }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.responsable')) }}
                                            :</strong> {{ $requestedProcedure->service_request->parent or trans('labels.na') }}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.admission-id')) }}
                                            :</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.contrast-allergy')) }}
                                            :</strong> {{ $requestedProcedure->patient->contrast_allergy ? ucfirst(trans('labels.yes')) : 'No' }}
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.medical-alerts')) }}
                                            :</strong> {{ $requestedProcedure->patient->medical_alerts }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ ucfirst(trans('labels.allergies')) }}: </strong>
                                        {{ $requestedProcedure->patient->allergies }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-file"
                                                       aria-hidden="true"></i> {{ ucfirst(trans('titles.procedure')) }}
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ $requestedProcedure->procedure->description }}</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.request-num') }}
                                            :</strong> {{ $requestedProcedure->service_request->id }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.admission-date')) }}:</strong> {{ App::getLocale() == 'es' ?
                                      date('d-m-Y', strtotime($requestedProcedure->service_request->issue_date)) : date('Y-m-d', strtotime($requestedProcedure->service_request->issue_date))
                                    }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.order-num') }}:</strong> {{ $requestedProcedure->id }}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.patient-type')) }}
                                            :</strong> {{ $requestedProcedure->service_request->patient_type->description }}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ ucfirst(trans('labels.source')) }}
                                            :</strong> {{ $requestedProcedure->service_request->source->description }}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.referring') . ': ' }}</strong>{{ $requestedProcedure->service_request->referring ? ucwords($requestedProcedure->service_request->referring->first_name . ' ' . $requestedProcedure->service_request->referring->last_name) : ucfirst(trans('labels.not-specified'))}}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.admitted-by') }}
                                            :</strong> {{ $requestedProcedure->service_request->user->first_name }} {{ $requestedProcedure->service_request->user->last_name }}
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.attended-by') }}
                                            :</strong> {{ $requestedProcedure->technician_user_name }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.dictated-by') }}
                                            :</strong> {{ $requestedProcedure->radiologist_user_name }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.transcribed-by') }}
                                            :</strong> {{ $requestedProcedure->transcriptor_user_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.contrast-study') . ': ' }}</strong>
                                        @if($requestedProcedure->procedure_contrast_study == 1)
                                            {{ trans('labels.yes')}}
                                        @else
                                            {{ trans('labels.not')}}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>
                                        <strong>{{ trans('labels.patient-abdominal-circumference') . ': ' }}</strong> {{ $requestedProcedure->abdominal_circumference or trans('labels.na') }}
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations_service_request') }}:</strong> {{ $requestedProcedure->service_request->comments }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations_procedure') }}:</strong> {{ $requestedProcedure->comments }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations_technician') }}:</strong> {{ $requestedProcedure->observation_technician }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p><strong>{{ trans('labels.observations_radiologist') }}:</strong> {{ $requestedProcedure->observation_radiologist }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel sombra">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.documents')) }}</h3>
                        </div>

                        <div class="panel-body">
                            <div class="k-widget k-upload k-header">
                                <ul class="k-upload-files k-reset">

                                    @foreach($requestedProcedure->patient->patientDocuments as $key => $patientDocument)
                                        <a href="{{ route('patients.document', ['document' => $patientDocument->id ]) }}"
                                           target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}"
                                                attr-id="{{ $patientDocument->id }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                <span class="k-file-extension">{{ $patientDocument->type }}</span>
                                                <span class="k-file-state"></span>
                                            </span>
                                                <span class="k-file-name-size-wrapper">
                                                <span class="k-file-name"
                                                      filename="{{ $patientDocument->filename }}">{{ $patientDocument->name }}</span>
                                                <span class="k-file-size">95.56 KB</span>
                                            </span>
                                            </li>
                                        </a>
                                    @endforeach

                                    @foreach($requestedProcedure->service_request->request_documents as $key => $requestDocument)
                                        <a href="{{ route('reception.document', ['document' => $requestDocument->id ]) }}"
                                           target="_blank">
                                            <li class="k-file" id="k-file-{{ $key + 100 }}">
                                                <span class="k-progress" style="width: 100%;"></span>
                                                <span class="k-file-extension-wrapper">
                                                <span class="k-file-extension">{{ $requestDocument->type }}</span>
                                                <span class="k-file-state"></span>
                                            </span>
                                                <span class="k-file-name-size-wrapper">
                                                <span class="k-file-name"
                                                      title="{{ $requestDocument->name }}">{{ $requestDocument->name }}</span>
                                                <span class="k-file-size">95.56 KB</span>
                                            </span>
                                            </li>
                                        </a>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="panel sombra">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#order-info"><h1
                                                    class="tab-title">{{ trans('labels.order-info') }}</h1></a></li>
                                    <li><a data-toggle="tab" href="#patient-history"><h1
                                                    class="tab-title">{{ trans('labels.patient-history') }}</h1></a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div id="order-info" class="tab-pane fade in active">

                                        <form action="{{ route('radiologist.approve', [$requestedProcedure->id]) }}"
                                              method='post' id="RadiologistApproveForm">
                                            <input id="reject" type="hidden" name="reject" value="0">
                                            <input id="reject_reason" type="hidden" name="reject_reason" value="">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div>
                                                        <label for="category-panel"></label>
                                                    </div>
                                                    <div class="panel panel-input-like" id="category-panel">
                                                        <div class="panel-heading" role="tab">
                                                            <a role="button" data-toggle="collapse" href="#search-teaching-file" aria-expanded="false" aria-controls="#search-teaching-file"
                                                            class="collapsed">
                                                                <h4 class="panel-title">
                                                                    {{ ucfirst(trans('labels.teaching-file')) }}
                                                                </h4>
                                                            </a>
                                                        </div>
                                                        <div id="search-teaching-file" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                        <div>
                                                                            <label for="category">{{ ucfirst(trans('labels.categories')) }}</label>
                                                                        </div>
                                                                        <select name="category_id" id="category_id">
                                                                            @foreach($categories as $category)
                                                                                <option value="{{ $category->id }}" {{ $category->id == $requestedProcedure->category_id ? 'selected' : '' }}>{{ $category->description }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                        <div>
                                                                            <label for="subcategory">{{ ucfirst(trans('labels.subcategories')) }}</label>
                                                                        </div>
                                                                        <select name="sub_category_id" id="sub_category_id" class="form-control">
                                                                            @foreach($subcategories as $subcategory)
                                                                                <option value="{{ $subcategory->id }}" {{ $subcategory->id == $requestedProcedure->sub_category_id ? 'selected' : '' }}>{{ $subcategory->description }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                        <div>
                                                                            <label for="teaching_file_text">{{ ucfirst(trans('labels.observations')) }}</label>
                                                                        </div>
                                                                        <div>
                                                                            <textarea class="form-control" name="teaching_file_text" id="teaching_file_text">{{ $requestedProcedure->teaching_file_text }}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>

                                            <div class="row">
                                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <a href="{{ route('radiologist.pac', ['id'=>$requestedProcedure->id]) }}"
                                                               class="btn btn-form btn-pac">
                                                                <span><i class="fa fa-picture-o"
                                                                         aria-hidden="true"></i> {{ trans('labels.open-images') }}</span>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <p><strong>Dictado:</strong></p>
                                                            <audio controls src="{{ $audio  }}">
                                                            </audio>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                            <div class="btn-icons">

                                                                <label for="template_id">{{ ucfirst(trans('labels.template')) }}
                                                                    :</label>

                                                                <select name="template_id" id="template_id">
                                                                    <option value="" selected
                                                                            disabled>{{ ucfirst(trans('labels.select')) }}</option>
                                                                    @foreach($requestedProcedure->procedure->templates as $template)
                                                                        <option value="{{ $template->id }}">{{ $template->description }}</option>
                                                                    @endforeach
                                                                </select>

                                                                @foreach($requestedProcedure->procedure->templates as $template)
                                                                    <input type="hidden"
                                                                           id="template_{{ $template->id }}"
                                                                           value="{{ $template->template }}">
                                                            @endforeach

                                                            <!--
                                                            <a href="javascript:;" id="insert-template" class="btn btn-form btn-icon" data-toggle="tooltip" title="{{ trans('labels.insert-template') }}">
                                                            <i class="fa fa-chevron-down"></i>
                                                        </a>
                                                    -->

                                                                <a href="{{ route('radiologist.edit.draft', [$requestedProcedure->id]) }}"
                                                                   id="btn-save-draft" class="btn btn-form btn-icon"
                                                                   data-toggle="tooltip"
                                                                   title="{{ trans('labels.save-draft') }}">
                                                                    <i class="fa fa-floppy-o"></i>
                                                                </a>

                                                            <!--
                                                    <a href="#" class="btn btn-form btn-icon" data-toggle="tooltip" title="{{ trans('labels.print-draft') }}">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            -->

                                                                <!--<a href="{{ route('radiologist.edit.draft', [$requestedProcedure->id]) }}"
                                                                   target="_blank" class="btn btn-form btn-icon"
                                                                   data-toggle="tooltip"
                                                                   title="{{ trans('labels.preview-draft') }}">
                                                                    <i class="fa fa-search"></i>
                                                                </a>
                                                                -->
                                                                <a id="download-file" class="btn btn-form btn-icon"
                                                                   data-toggle="tooltip"
                                                                   title="{{ trans('labels.view-file-report') }}"
                                                                   href="">
                                                                    <i class="fa fa-file-pdf-o"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row m-t-20">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 big-textarea-plantilla">
                                                            <textarea class="" name="text"
                                                                      id="template">{{ $requestedProcedure->text }}</textarea>
                                                            @if ($errors->has('text'))
                                                                <span class="help-block error-help-block">{{ $errors->first('text') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                    <div id="patient-history" class="tab-pane fade">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="mdl_preview"
         class="modal fade bd-example-modal-lg"
         tabindex="-1"
         role="dialog"
         aria-labelledby="myLargeModalLabel"
         aria-hidden="true"
         style="z-index: 9999;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-danger pull-right"
                                    id="btnClose">
                                <i class="fa fa-close"></i>
                            </button>
                        </div>
                        <div class="col-12">
                            <iframe frameborder="0"
                                    width="100%;"
                                    height="700px;"
                                    id="frame_pdf"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.modal-reject')
    <script>
        $.get('/servicerequests/history/' + {{ $requestedProcedure->patient->id }} , function (response) {
            $('#patient-history').append(response);
        });
        $(document).ready(function ($) {
            $('#download-file').on('click', function (e) {
                var editor = $("#template").data("kendoEditor");
                var editorContent = editor.value();
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('finalreport.pdfpreview') }}",
                    method: 'post',
                    data: {
                        id:{{$requestedProcedure->id}},
                        content: editorContent
                    },
                    success: function (result) {
                        $("#frame_pdf").attr("src", result.src);

                        $("#mdl_preview").modal("show");
                    }
                });
            });

            $("body").on("click", "#btnClose", function () {
                $("#mdl_preview").modal("hide");
            });

        });
    </script>

@endsection
