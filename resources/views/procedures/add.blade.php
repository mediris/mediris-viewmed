@extends('layouts.app')

@section('title',ucfirst(trans('titles.add')).' '.trans('titles.procedure'))

@section('content')

@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.add')).' '.trans('titles.procedure'),
                                'elem_type' => 'button',
                                'elem_name' => ucfirst(trans('labels.save')),
                                'form_id' => '#prodecure-add-form',
                                'route' => '',
                                'fancybox' => '',
                                'routeBack' => route('procedures')
                            ])

    <div class="container-fluid" xmlns="http://www.w3.org/1999/html">

        @if(Session::has('message'))
        <div class="{{ Session::get('class') }}">'
            <p>{{ Session::get('message') }}</p>
        </div>
        @endif

        <form id="prodecure-add-form" method="post" action="{{ route('procedures.add') }}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="row">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>


            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <!-- MAIN INFORMATION -->
                    <div class="panel sombra x4 elastic">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('labels.main-info') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'active-chk',
                                        'name'      =>'active',
                                        'label'     =>'labels.is-active',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('active'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div>
                                        <label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" name="administrative_ID" id="units"
                                        class="input-field form-control user btn-style"
                                        value="{{ old('administrative_ID') }}"/>
                                    </div>
                                    @if ($errors->has('administrative_ID'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-xs-6">
                                    <div>
                                        <label for='templates'>{{ ucfirst(trans('labels.modality')) }}</label>
                                    </div>
                                    @include('includes.select', [
                                        'idname' => 'modality_id',
                                        'data'   => $modalities,
                                        'keys'   => ['id', 'name'],
                                    ])
                                    
                                    @if ($errors->has('modality_id'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('templates') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        <label for='templates'>{{ ucfirst(trans('labels.templates')) }}</label>
                                    </div>
                                    <select class="form-control" name="templates[]" id="templates[]" data-placeholder="{{ ucfirst(trans('labels.templates')) }}" multiple="multiple">
                                        @foreach($templates as $template)
                                        <option value="{{ $template->id }}">{{ $template->description }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('templates'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('templates') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div>
                                        <label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="text" name="description" id="description" rows="5" class="input-field form-control user btn-style" value="{{ old('description') }}"/>
                                    </div>
                                    @if ($errors->has('description'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- GENERAL INFORMATION -->
                    <div class="panel sombra x3 x3-less">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.general-info')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'interview',
                                        'name'      =>'interview',
                                        'label'     =>'labels.needs-interview',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('interview'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('interview') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'worklist',
                                        'name'      =>'worklist',
                                        'label'     =>'labels.needs-worklist',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('worklist'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('worklist') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'images',
                                        'name'      =>'images',
                                        'label'     =>'labels.needs-images',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('images'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'mammography',
                                        'name'      =>'mammography',
                                        'label'     =>'labels.needs-mammography',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('mammography'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('mammography') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'radiologist',
                                        'name'      =>'radiologist',
                                        'label'     =>'labels.needs-radiologist',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('radiologist'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('radiologist') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    @include('includes.general-checkbox', [
                                        'id'        =>'technician',
                                        'name'      =>'technician',
                                        'label'     =>'labels.needs-technician',
                                        'condition' => 0
                                    ])
                                    @if ($errors->has('technician'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('technician') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PROCEDURE'S STEPS -->
                    <div class="panel sombra x3 x3-less">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('labels.proc-steps-created') }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div>
                                        <label for='selected'>{{ ucfirst(trans('labels.steps')) }}</label>
                                    </div>
                                    <select class="form-control" name="selected" id="step-selected">
                                        <option value="" disabled>{{ ucfirst(trans('labels.select')) }}</option>
                                        @foreach($steps as $step)
                                        <option value="{{ $step->id }}">{{ $step->description }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('selected'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('selected') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 m-t-30">
                                    <div class="row text-center">
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <a href="javascript:;" class="btn btn-form btn-left" id="legend-add-created-procedure">
                                                {{ ucfirst(trans('titles.select')).' '.trans('titles.step') }}</a>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <a href="javascript:;" class="btn btn-form btn-left" id="legend-add-procedure">
                                                {{ ucfirst(trans('titles.create')) }}
                                                {{ trans('titles.new') }}
                                                {{  trans('titles.step') }}</a>
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                            <a href="javascript:;" class="btn btn-danger" id="legend-delete-procedure">
                                                {{ ucfirst(trans('titles.delete')) }}
                                                {{ trans('titles.last-sm') }}
                                                {{  trans('titles.step') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <!-- PROCEDURE'S INDICATIONS -->
                    <div class="panel sombra x4" >
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.procedure-info')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <label for='indications'>{{ ucfirst(trans('labels.indications')) }}</label>

                                    <textarea class="form-control" name="indications" rows="9" id="indications">{{ old('indications') }}</textarea>
                                    
                                    @if ($errors->has('indications'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('indications') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ADMIN INFORMATION -->
                    <div class="panel sombra x3 x3-less">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ ucfirst(trans('labels.admin-info')) }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div>
                                        <label for="radiologist_fee">{{ ucfirst(trans('labels.radiologist-fee')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="number" min="0" step="0.1" class="form-control" name="radiologist_fee" id="radiologist_fee" value="0">
                                    </div>
                                    
                                    @if ($errors->has('radiologist_fee'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('radiologist_fee') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div>
                                        <label for="technician_fee">{{ ucfirst(trans('labels.technician-fee')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="number" min="0" step="0.1" class="form-control" name="technician_fee" id="technician_fee" value="0">
                                    </div>
                                    @if ($errors->has('technician_fee'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('technician_fee') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div>
                                        <label for="transcriptor_fee">{{ ucfirst(trans('labels.transcriptor-fee')) }} *</label>
                                    </div>
                                    <div>
                                        <input type="number" min="0" step="0.1" class="form-control" name="transcriptor_fee" id="transcriptor_fee" value="0">
                                    </div>
                                    @if ($errors->has('transcriptor_fee'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('transcriptor_fee') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- CREATED STEPS -->
                    <div class="panel sombra x3 x3-less elastic">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6 col-sm-9 col-md-9 col-lg-9">
                                    <h3 class="panel-title">{{ trans('labels.procedure-steps') }}</h3>
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                    <h3 class="badge" id="steps-count">0</h3>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="steps" class="steps panel-group" role="tablist" aria-multiselectable="true">
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
        {!! JsValidator::formRequest('App\Http\Requests\ProcedureAddRequest', '#prodecure-add-form'); !!}
    </div>
    @endsection
