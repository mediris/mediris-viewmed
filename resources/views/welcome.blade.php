@extends('layouts.app')

@section('title', ucfirst(trans('titles.home')))

@section('content')



    <div class="container-fluid" id="home">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-carrousel panel-stat sombra">

                    @if(Session::has('message'))
                        <div class="{{ Session::get('class') }}">
                            <ul>
                                <li>{{ Session::get('message') }}</li>
                            </ul>
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div id="myCarousel" class="carousel slide" data-interval="30000" data-ride="carousel">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="active item">
                                <img src="/images/banner.jpg" alt="First Slide">
                            </div>
                            <div class="item">
                                <img src="/images/banner3.jpg" alt="Second Slide">
                            </div>
                            <div class="item">
                                <img src="/images/banner2.jpg" alt="Third Slide">
                            </div>
                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="carousel-control right" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if (count($alertmessages) > 0)
            <div id="messageCarousel" class="carousel slide" data-interval="15000" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel sombra panel-stat" id="message_panel">
                            <div class="panel-body bg-white">
                                <div class="carousel-inner">
                                    @foreach ($alertmessages as  $key => $message)
                                        <div class="item row {{ $key == 0 ? 'active' : '' }}">
                                            <div class="mail-image col-md-2 align-center">
                                                <div class="alert-content {{ $message->alertMessageType->css_class }}">

                                                </div>
                                            </div>
                                            <div class="message_board col-md-10">
                                                <p>{{ $message->message }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <!-- Carousel indicators -->
                                <ol class="carousel-indicators">
                                    @foreach ($alertmessages as  $key => $message)
                                        <li data-target="#messageCarousel" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row m-b-80 m-t-60 charts">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel panel-stat sombra bd-0">
                    <div class="panel-heading no-bd bg-blue-m">
                        <h3 class="panel-title">{{trans('titles.statistics')}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="legend"><h4 class=" leyenda1 text-left c-gray">Solicitudes admitidas <span>{{ $totalServiceRequests }}</span>
                                    </h4></div>
                                <div class="legend"><h4 class=" leyenda2 text-left c-gray">Ordenes dictadas
                                        <span>{{ $totalOrdersToTranscribe }}</span></h4></div>
                                <div class="legend"><h4 class=" leyenda3 text-left c-gray">Ordenes aprobadas
                                        <span>{{ $totalOrdersFinished }}</span></h4></div>
                                <div class="legend"><h4 class=" leyenda4 text-left c-gray">Ordenes realizadas
                                        <span>{{ $totalOrdersToDictate }}</span></h4></div>
                                <div class="legend"><h4 class=" leyenda5 text-left c-gray">Ordenes transcritas
                                        <span>{{ $totalOrdersToApprove }}</span></h4></div>
                            </div>-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="demo-section k-content wide">
                                    <div id="global-chart"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel panel-stat sombra bd-0">
                    <div class="panel-heading no-bd bg-blue-m">
                        <h3 class="panel-title">{{ App::getLocale() == 'es' ? trans('months.' . date('n')) : date('F') }}</h3>
                    </div>
                    
                    <div class="panel-body">
                        <div class="row">
                            <!--<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 align-center">
                                <div class="legend">
                                    <h4 class="leyenda1 text-left c-gray">Solicitudes admitidas <span></span></h4>
                                </div>
                                <div class="legend">
                                    <h4 class=" leyenda2 text-left c-gray">Ordenes dictadas<span>4</span></h4></div>
                                <div class="legend"><h4 class=" leyenda3 text-left c-gray">Ordenes aprobadas
                                        <span>3</span></h4></div>
                                <div class="legend"><h4 class=" leyenda4 text-left c-gray">Ordenes realizadas
                                        <span>7</span></h4></div>
                                <div class="legend"><h4 class=" leyenda5 text-left c-gray">Ordenes transcritas
                                        <span>2</span></h4></div>
                            </div>-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="demo-section k-content wide">
                                    <div id="month-chart"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel panel-stat sombra bd-0">
                    <div class="panel-heading no-bd bg-blue-m">
                        <h3 class="panel-title">{{trans('titles.personal-statistics')}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 align-center">
                                <div class="legend"><h4 class=" leyenda1 text-left c-gray">Solicitudes admitidas <span>25</span>
                                    </h4></div>
                                <div class="legend"><h4 class=" leyenda2 text-left c-gray">Ordenes dictadas
                                        <span>4</span></h4></div>
                                <div class="legend"><h4 class=" leyenda3 text-left c-gray">Ordenes aprobadas
                                        <span>3</span></h4></div>
                                <div class="legend"><h4 class=" leyenda4 text-left c-gray">Ordenes realizadas
                                        <span>7</span></h4></div>
                                <div class="legend"><h4 class=" leyenda5 text-left c-gray">Ordenes transcritas
                                        <span>2</span></h4></div>
                            </div> -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="demo-section k-content wide">
                                    <div id="user-chart"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function createCharts() {
            $("#global-chart").kendoChart({
                title: {
                    position: "bottom",
                    text: "{{ trans('titles.global-statistics') }}"
                },
                legend: {
                    position: "left"
                },
                chartArea: {
                    background: ""
                },
                series: [{
                    type: "pie",
                    overlay: {
                        gradient: "none"
                    },
                    data: [{
                        category: "{{ trans('labels.orders-to-admit') }}",
                        value: {{ $totalOrdersToAdmit }},
                        color: "#0079c2"
                    },{
                        category: "{{ trans('labels.orders-to-do') }}",
                        value: {{ $totalOrdersToDo }},
                        color: "#37a1d5"
                    },{
                        category: "{{ trans('labels.orders-to-dictate') }}",
                        value: {{ $totalOrdersToDictate }},
                        color: "#1c3e4e"
                    },{
                        category: "{{ trans('labels.orders-to-transcribe') }}",
                        value: {{ $totalOrdersToTranscribe }},
                        color: "#a4a9ae"
                    },{
                        category: "{{ trans('labels.orders-to-approve') }}",
                        value: {{ $totalOrdersToApprove }},
                        color: "#a94442"
                    },{
                        category: "{{ trans('labels.orders-finished') }}",
                        value: {{ $totalOrdersFinished }},
                        color: "#70858e"
                    },{
                        category: "{{ trans('labels.orders-suspended') }}",
                        value: {{ $totalOrdersSuspended }},
                        color: "#203d4f"
                    }]
                }],
                tooltip: {
                    visible: true,
                    template: "#= category #: \n #= value #"
                }
            });

            $("#month-chart").kendoChart({
                title: {
                    position: "bottom",
                    text: "{{ trans('titles.month-statistics') }}"
                },
                legend: {
                    position: "left"
                },
                chartArea: {
                    background: ""
                },
                series: [{
                    type: "pie",
                    overlay: {
                        gradient: "none"
                    },
                    data: [{
                        category: "{{ trans('labels.orders-to-admit') }}",
                        value: {{ $totalOrdersToAdmitByMonth }},
                        color: "#0079c2"
                    },{
                        category: "{{ trans('labels.orders-to-do') }}",
                        value: {{ $totalOrdersToDoByMonth }},
                        color: "#37a1d5"
                    },{
                        category: "{{ trans('labels.orders-to-dictate') }}",
                        value: {{ $totalOrdersToDictateByMonth }},
                        color: "#1c3e4e"
                    },{
                        category: "{{ trans('labels.orders-to-transcribe') }}",
                        value: {{ $totalOrdersToTranscribeByMonth }},
                        color: "#a4a9ae"
                    },{
                        category: "{{ trans('labels.orders-to-approve') }}",
                        value: {{ $totalOrdersToApproveByMonth }},
                        color: "#a94442"
                    },{
                        category: "{{ trans('labels.orders-finished') }}",
                        value: {{ $totalOrdersFinishedByMonth }},
                        color: "#70858e"
                    },{
                        category: "{{ trans('labels.orders-suspended') }}",
                        value: {{ $totalOrdersSuspendedByMonth }},
                        color: "#203d4f"
                    }]
                }],
                tooltip: {
                    visible: true,
                    template: "#= category #: \n #= value #"
                }
            });

            $("#user-chart").kendoChart({
                title: {
                    position: "bottom",
                    text: "{{ trans('titles.personal-statistics') }}"
                },
                legend: {
                    position: "left"
                },
                chartArea: {
                    background: ""
                },
                series: [{
                    type: "pie",
                    overlay: {
                            gradient: "none"
                        },
                    data: [{
                        category: "{{ trans('labels.orders-done') }}",
                        value: {{ $totalOrdersDoItByUser }},
                        color: "#0079c2"
                    },{
                        category: "{{ trans('labels.orders-issued') }}",
                        value: {{ $totalOrdersDictatedByUser }},
                        color: "#37a1d5"
                    },{
                        category: "{{ trans('labels.orders-transcribed') }}",
                        value: {{ $totalOrdersTranscribedByUser }},
                        color: "#1c3e4e"
                    },{
                        category: "{{ trans('labels.orders-approved') }}",
                        value: {{ $totalOrdersApprovedByUser }},
                        color: "#a4a9ae"
                    }]
                }],
                tooltip: {
                    visible: true,
                    template: "#= category #: \n #= value #"
                }
            });
        }

        $(document).ready(createCharts);
        $(document).bind("kendo:skinChange", createCharts);
    </script>
@endsection
