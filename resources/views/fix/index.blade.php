@extends('layouts.app')

@section('title', ucfirst(trans('Corregir')))

@section('content')

@include('partials.actionbar',[ 'title' => ucfirst(trans('Corregir Cedula')),
'form_id' => '',
'fancybox' => '',
])

<div class="container-fluid search edit">
<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if(Session::has('message'))
                    <div class="{{ Session::get('class') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>

    <form method="post" id="form-orden" action="{{route("fix.edit")}}">
        <div class="form-group row">
            <div class="col-md-2"></div>
                <div class="col-md-5">
                    <label>Nro. Orden a modificar</label>
                        <input type="number" class="form-control" id="orden" name="orden">
                        <button type="submit" class="btn btn-primary float-right btn-form-cedula color-primary">Buscar</button>
                </div>
        </div>
    </form>
</div>
@endsection
