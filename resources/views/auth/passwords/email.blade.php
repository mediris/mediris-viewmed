@extends('layouts.app')

@section('title', ucfirst(trans('titles.reset'))." ".ucfirst(trans('titles.password')))

<!-- Main Content -->
@section('content')
    <div class="container" id="login-block">
        <div class="row">
            <div class="logo">
                <a href="{{ url('/login') }}"><img src="/images/logo_mediris.png" alt="Login"></a>
            </div>
            <div class="col-sm-6 col-md-6 col-sm-offset-3">
                <div class="login-box clearfix">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- BEGIN ERROR BOX -->
                    @if (count($errors) > 0)
                        <!--<div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>-->
                    @endif
                    <!-- END ERROR BOX -->
                    <div class="login-logo">
                        <img src="/images/mediris_icon.png" alt="MEDIRIS Logo">
                    </div>
                    <div class="login-form ">
                        <form method="post" action="{{ url('/password/email') }}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for='email' class="m-t-20">{{ ucfirst(trans('messages.lbl-email')) }}</label>
                                <input type="email" name="email" id="email" class="input-field form-control user m-b-20 btn-style {{ $errors->has('email') ? 'alert alert-danger' : '' }}" value="{{ old('email') }}"/>
                                @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                                <div class="btn-caja">
                                    <button id="submit-form" class="btn btn-login ladda-button" data-style="expand-left">
                                        <span class="ladda-label">{{ ucfirst(trans('messages.btn-send')) }}</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection