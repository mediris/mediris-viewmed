@extends('layouts.app')

@section('title',ucfirst(trans('titles.edit')).' '.trans('titles.room'))

@section('content')
	
	@include('partials.actionbar',[ 'title' => ucfirst(trans('titles.edit')).' '.trans('titles.room'),
									'elem_type' => 'button',
									'elem_name' => ucfirst(trans('labels.save')),
									'form_id' => '#RoomEditForm',
									'route' => '',
									'fancybox' => '',
									'routeBack' => route('rooms')
								])
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				@if(Session::has('message'))
					<div class="{{ Session::get('class') }}">
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif

				@if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
			</div>
		</div>
		
		<form action="{{ route('rooms.edit', [$room->id]) }}" method="post" id="RoomEditForm">
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="panel sombra x4">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-hospital-o"
							                           aria-hidden="true"></i> {{ ucfirst(trans('titles.room')) }}</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									@include('includes.general-checkbox', [
									    'id'        =>'active-chk',
									    'name'      =>'active',
									    'label'     =>'labels.is-active',
									    'condition' => $room->active
									])
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='name'>{{ ucfirst(trans('labels.name')) }} *</label>
									</div>
									<div>
										<input type="text" class="form-control" name="name" id="name"
										       value="{{ old('name', $room->name) }}">
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div>
										<label for='administrative_ID'>{{ ucfirst(trans('labels.administrative-id')) }}
											*</label>
									</div>
									<div>
										<input type="text" class="form-control" name="administrative_ID"
										       id="administrative_ID" value="{{ old('administrative_ID', $room->administrative_ID) }}">
									</div>
								</div>
							
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label class="control-label"
										       for="division_id">{{ ucfirst(trans('labels.division')) }} *</label>
									</div>
									<div>
										@include('includes.select', [
                                            'idname' => 'division_id',
                                            'data' 	 => $divisions,
                                            'keys' 	 => ['id', 'name'],
                                            'value'  => old('division_id', $room->division_id )
                                        ])
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div>
										<label for='description'>{{ ucfirst(trans('labels.description')) }} *</label>
									</div>
									<div>
										<textarea class="form-control" name="description" id="description"
										          rows="5">{{ old('description',$room->description) }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="panel sombra x4">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-file-text"
							                           aria-hidden="true"></i> {{ ucfirst(trans('titles.procedures')) }}
							</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="modality_id_reception">{{ ucfirst(trans('labels.modality')) }}</label>
									</div>
									<div>
										@include('includes.select', [
                                            'idname' => 'modality_id_reception',
                                            'data' => $modalities,
                                            'keys' => ['id', 'name'] 
                                        ])
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="procedure_id">{{ ucfirst(trans('labels.procedures')) }}</label>
									</div>
									<div>
										@include('includes.procedures-select',[
                                                        'idname' => 'procedure_id',
                                                        'procedures' => $procedures
                                                    ])
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="default_duration">{{ ucfirst(trans('labels.default-duration')) }}</label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="default_duration" id="default_duration" value="1">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<div class="inline-button">
										<a type="button" href="javascript:;" name="edit-photo" id="addProcedureRoom"
										   class="btn btn-form" title="Edit avatar">
											<i class="fa fa-plus" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.add')) }}
										</a>
										
										<a href="javascript:;" class="btn btn-form" id="addAllProcedureRoom">
											<i class="fa fa-plus" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.select-all')) }}
										</a>

										<a href="javascript:;" class="btn btn-form btn-danger" id="removeProcedureRooms">
											<i class="fa fa-minus" aria-hidden="true"></i>
											{{ ucfirst(trans('labels.remove-all')) }}
										</a>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					
					<div class="panel sombra">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-calendar" aria-hidden="true"></i> Bloques</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
									<div>
										<label for='start_hour'>Hora de inicio *</label>
									</div>
									<div>
										<input type="text" class="" name="start_hour" id="start_hour"
										       value="{{ old('start_hour', $room->start_hour) }}">
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
									<div>
										<label for='end_hour'>Hora de fin *</label>
									</div>
									<div>
										<input type="text" class="" name="end_hour" id="end_hour"
										       value="{{ old('end_hour', $room->end_hour) }}">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for="block_size">Tama&ntilde;o del bloque *</label>
									</div>
									<div>
										@include('includes.select', [
                                            'idname' => 'block_size',
                                            'data' => $blockSizes,
                                            'keys' => [0, 1],
                                            'value'  => old('block_size', $room->block_size ) 
                                        ])
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for='quota'>Cupos por Bloque *</label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="quota" id="quota" value="{{ $room->quota }}">
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div>
										<label for='equipment_number'>N&uacute;mero de Equipos *</label>
									</div>
									<div>
										<input type="number" min="1" class="form-control" name="equipment_number" id="equipment_number" value="{{ $room->equipment_number }}">
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="panel sombra">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-calendar-times-o" aria-hidden="true"></i> Bloques
								inhabilitados</h3>
						</div>
						<div class="panel-body">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<div>
									<label for="block_size">Bloques</label>
								</div>
								<div id="lock_blocks">
								
								</div>
							
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="row">
						<div id="procedures">
						@foreach($room->procedures as $key => $procedure)
							@include( 'procedures.form', [
								'position' => rand(2500, 5000000),
								'procedure' => $procedure,
								'duration' => $procedure->pivot->duration
							])
						@endforeach
						</div>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
		{!! JsValidator::formRequest('App\Http\Requests\RoomEditRequest', '#RoomEditForm'); !!}
	</div>
	
	<script>
		
		//Pre load the blocks lockeds
		
		var avoided_blocks = '<?php  echo $room->avoided_blocks ?>';
		if ( avoided_blocks == '' ) {
			avoided_blocks = [];
		} else {
			avoided_blocks = avoided_blocks.trim().split(" ");
		}

		var start_hour = moment($('#start_hour').val(), "h:mm A");
		var end_hour = moment($('#end_hour').val(), "h:mm A");
		var block_size = $('#block_size').val();
		
		var select = "";
		var block_number = 0;
		var html = '<select class="form-control" name="blocks_locked[]" id="blocks_locked" multiple>';
		while (start_hour.format("h:mm A") != end_hour.format("h:mm A")) {
			select = "";
			
			for (var i = 0; i < avoided_blocks.length; i++) {
				if (block_number == avoided_blocks[i]) {
					select = "selected";
					break;
				}
			}
			
			html = html + '<option value="' + block_number + '" ' + select + '>' + start_hour.format("h:mm A") + ' - ' + start_hour.add(block_size, "m").format("h:mm A") + '</option>';
			
			block_number++;
		}
		
		html = html + '</select>';
		
		$('#lock_blocks').append(html);
		
		//Pre load the blocks lockeds
	</script>
	



@endsection