<div class="search">
    <form method="post" action="">
        {!! csrf_field() !!}
        <fieldset class="m-b-40">

            <div class="row">
                <div class="col-md-3">
                    <div>
                        <label for='inpt_search'>{{ ucfirst(trans('messages.search')) }}</label>
                    </div>
                    <div>
                        <input type="text" name="search" id="inpt_search" class="input-field form-control user btn-style"/>
                    </div>
                </div>

                <div class="col-md-3">
                    <br>
                    <div>
                        <button id="btn-basic-search" class="btn btn-form ladda-button" data-style="expand-left" type="submit"><span class="ladda-label">{{ ucfirst(trans('messages.search')) }}</span></button>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>