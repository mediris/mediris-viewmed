<script>
	function AlertStatusOrder(blocked_status) {
        if(blocked_status == '1')
        {
            return "{!! trans('labels.blocked-order') !!}";
        }
        else
        {
            return "{!! trans('labels.unlocked-order') !!}";
        }
    }

	function appointmentMessage() {
        return "{{ ucfirst(trans('titles.appointment')) }}";
    }

	function BlockLocked() {
        return "{{ trans('labels.block-locked') }}";
    }

	function confirmDelete() {
        $('.delete-link').click(function(){
            return kendo.confirm('<?php echo ucfirst(trans('alerts.confirm-delete-row')); ?>');
        });
    }

    function confirmMessage() {
        return "{{ trans('alerts.confirm') }}";
    }

    function ConfirmTemplateInsertion() {
        return "{!! trans('alerts.confirm-template') !!}";
    }

    function DateBiggerThanNow() {
        return "{{ trans('errors.bigger-than-now') }}";
    }

    function deathDateMessage() {
        return"{{ ucfirst(trans('messages.death-date')) }}"
    }

    function EquipmentText() {
        return "{{ ucfirst(trans('labels.equipment')) }}";
    }

    function error5001() {
		return "{!! trans('errors.5001') !!}";
	}
	
	function error5002() {
		return "{!! trans('errors.5002') !!}";
	}
	
	function errorAddProcedure($same) {   
        if($same){
            kendo.alert('<?php echo ucfirst(trans('alerts.same-procedure')) ?>') 
        }else{
           kendo.alert('<?php echo ucfirst(trans('alerts.select-valid-procedure')) ?>') 
        }    
    }

	function errorAddStep() {
		kendo.alert('<?php echo ucfirst(trans('alerts.select-valid-step')) ?>')
	}

	function errorAppointmentProcedure() {
		return "{!! trans('validation.required', ['attribute' => trans('validation.attributes.procedure_id')]) !!}";
	}

	function errorBusyBlock() {
		return "{!! trans('alerts.busy-block') !!}"
	}

	function errorDeleteBlock() {
		return "{{ trans('alerts.delete-block') }}";
	}

	function errorEditBlock() {
		return "{{ trans('alerts.edit-block') }}"
	}
	
	function errorInsufficientBlock() {
		return "{{ trans('alerts.insufficient-block') }}";
	}

	function ErrorMessageForm(rule, field) {
		console.log (field);

		if( field==undefined ) {
			return "{!! trans('alerts.all-required') !!}" ;
		} else {
			return "{!! trans('alerts.field') !!} " + field + " {!! trans('alerts.field-required') !!}" ;	
		}
	}
	
	function errorMoveBlock() {
		return "{{ trans('alerts.move-block') }}";
	}

	function errorNotValidDate() {
		return "{{ trans('alerts.wrong-date') }}";
	}

    function errorRoomNotReady() {
		return "{!! trans('alerts.room-not-ready') !!}";
	}
	
	function errorUnknown() {
        return "{!! trans('errors.unknown') !!}";   
    }

    function getFormatDate() {
        @if (App::getLocale() == 'es')
            return 'yyyy-MM-dd';
        @else
            return 'yyyy-MM-dd';
        @endif
    }

    function killSessionMessage() {
        return "{{ trans('alerts.kill-session') . " " . trans('alerts.confirm-action') }}";
    }

    function NotAuthorized() {
        return "{{ trans('alerts.forbidden') }}";
    }

    function noConfirm() {
        return "{{ trans('labels.not') }}";
    }

    function NoReverse(){
        return "{!! trans('alerts.no-reverse') !!}";
    }

    function NumberOrderToApprove(n) {
        return "{!! trans('messages.approve-orders', ['number' => '"+n+"']) !!}";
    }

    function ProvideSearchCriteria() {
        return "{!! trans('alerts.provide-criteria') !!}";
    }

    function reasonToDelete() {
        return "{!! trans('alerts.reason-to-delete') !!}";
    }

    function RefreshError() {
		return "{{ trans('errors.refreshing') }}";
	}

    function RewriteWorklist() {
        return "{!! trans('labels.rewrite-worklist') !!}";
    }

	function SelectValidTemplate() {
        return "{!! trans('alerts.select-valid-template') !!}";
    }
    
    function SetAnonymous() {
        return "{{ ucfirst(trans('labels.anonymous')) }}";
    }
	
    function SuccessAppointmentCanceled() {
        return "{{ trans('alerts.appointment-discharged') }}";
    }

    function SuccessAppointmentCreated() {
        return "{{ trans('alerts.appointment-created') }}";
    }

    function SuccessAppointmentEdited() {
        return "{{ trans('alerts.appointment-edited') }}";
    }

    function TitleOrderToApprove() {
        return "{!! trans('labels.orders-pre-entry') !!}";
    }

    function WorklistTittle() {
        return "{!! trans('labels.worklist') !!}";
    }

    function WrongReasonToCancel() {
        return "{{ trans('alerts.wrong-reason') }}";
    }
    
    function WrongRoomEndHour() {
        return "{{ trans('alerts.wrong-room-hour') }}";
    }

    function yesConfirm() {
        return "{{ trans('labels.yes') }}";
    }

</script>
