@if(Auth::user()->hasRole(1) || Auth::user()->id == 1)
    <li class="{{ (Request::segment(1) == 'actions' ||  Request::segment(1) == 'institutions' ||
			   					Request::segment(1) == 'modalities' || Request::segment(1) == 'roles' ||
			   					Request::segment(1) == 'sections' || Request::segment(1) == 'users')? 'current active' : ''}}">
        <a href="#" target="_self">
            <i class="ico icon-sistema"></i>
            <span class="sidebar-text ">{{ ucfirst(trans('labels.system')) }}</span>
            <span class="arrow glyphicon glyphicon-triangle-right
								{{ (Request::segment(1) == 'actions' ||  Request::segment(1) == 'institutions' ||
									Request::segment(1) == 'modalities' || Request::segment(1) == 'roles' ||
									Request::segment(1) == 'sections' || Request::segment(1) == 'users')? 'arrow-down' : ''}}" id="arrow-sistem">
								</span>
        </a>
        <ul class="submenu collapse">
            <li><a href="{{ route('institutions') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.institutions')) }}</span></a></li>
            <li><a href="{{ route('migrations') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.migrations')) }}</span></a></li>
            <li><a href="{{ route('modalities') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.modalities')) }}</span></a></li>
            <li><a href="{{ route('roles') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.roles')) }}</span></a></li>
            <li><a href="{{ route('users') }}"><span class="sidebar-text">{{ ucfirst(trans('labels.users')) }}</span></a></li>
        </ul>
    </li>
@endif