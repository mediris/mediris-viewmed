<li class="{{ (Request::segment(1) == 'sources' || Request::segment(1) == 'templates')? 'current active' : ''}}">
    <a href="#">
        <i class="ico icon-administracion"></i>
        <span class="sidebar-text ">{{ ucfirst(trans('labels.administration')) }}</span>
        <span class="arrow glyphicon glyphicon-triangle-right
                                			{{ (Request::segment(1) == 'sources' ||
												Request::segment(1) == 'templates') ? 'arrow-down' : ''}}"
              id="arrow-adm"></span>
    </a>

    <ul class="submenu collapse">
