@if(count($value) > 1)
	<li>
	    <a class="docent-file-link" href="{{ $route }}">
	        <span class="sidebar-text">{{ ucfirst(trans('labels.'.$key)) }}</span>
	    </a>
	</li>
@endif