<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suffix extends Model {
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Suffix tiene muchos Users.
     */
    public function users() {
        return $this->hasMany(User::class);
    }
}
