<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\AlertMessage;
use App\AlertMessagesUser;

class Kernel extends ConsoleKernel{

    protected $commands = [
        Commands\CleanAlertMessages::class,
        Commands\SendBIRADSNotifications::class,
        Commands\DeleteImagesTemp::class,
        Commands\SetAppointmentStatusNoShow::class,
        Commands\GenerateToken::class,
        Commands\KillSessions::class,
    ];


    protected function schedule(Schedule $schedule){
        $schedule->command('clean:alertmessages')->daily();
        $schedule->command('clean:delete-images-temp')->daily();
        $schedule->command('send:biradsNotifications')->daily();
        $schedule->command('setAppointmentStatus:noShow')->daily();
        $schedule->command('queue:work --once')->everyMinute();
    }
}