<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AlertMessagesUser;
use DB;

class CleanAlertMessages extends Command
{

    protected $signature = 'clean:alertmessages';
    protected $description = 'Limpia mensajes de alerta leidos';


    public function __construct(){
        parent::__construct();
    }


    public function handle(){
        AlertMessagesUser::join(DB::raw('(SELECT id FROM alert_messages WHERE end_date < now()) AS old'), function($join){
                $join->on('alert_messages_users.alert_message_id', '=', 'old.id');
        })->delete();
    }
}
