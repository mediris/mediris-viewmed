<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertMessageType extends Model {

    protected $fillable = [
        'name',
        'icon',
        'css_class',
        'priority',
        'active',
    ];

    public function alertMessages() {
        return $this->hasMany(AlertMessage::class);
    }
}
