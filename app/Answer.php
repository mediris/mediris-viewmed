<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class Answer extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/answers';
        parent::__construct();
    }

    static function remoteFindAll( $where = [],$institution = null ) {
        $ret = parent::remoteFindAll( $where ,$institution);
        
        /************* Translation and sorting ***************/

        foreach ( $ret as $key => $answer ) {
            $ret[$key]->description = trans('statuses.' . $answer->description);
        }
        if(!is_array($ret)){
            $answers = $ret->toArray();
        }else{
            $answers = $ret;
        }

        usort($answers, function ( $a, $b ) {

            if ( $a['description'] == $b['description'] ) {
                return 0;
            }

            return ( $a['description'] < $b['description'] ) ? -1 : 1;
        });

        /*****************************************************/

        return $answers;
    }
}
