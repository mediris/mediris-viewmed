<?php
 
namespace App\Http\Middleware;
 
use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Response;
 
class CheckForMaintenanceMode
{
 
    /**
    * The application implementation.
    *
    * @var \Illuminate\Contracts\Foundation\Application
    */
    protected $app;
 
    /**
    * Create a new middleware instance.
    *
    * @param \Illuminate\Contracts\Foundation\Application $app
    * @return void
    */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
 
    /**
    *
    * Routes for exception
    *
    */
    protected $except =
    [
        'institutions/updatelicenseDate/*',
        'institutions/licenseValidated'

    ];
 
    /**
    *
    * IPS for exception
    *
    */
    protected $ips =
    [
        '255.255.255.255'
    ];
 
    /**
    * Handle an incoming request.
    *
    * @param \Illuminate\Http\Request $request
    * @param \Closure $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
 
        if ($this->app->isDownForMaintenance() && !in_array($request->getClientIp(), $this->ips))
        {
            //Bucle for routes
            foreach ($this->except as $except) {
                if ($except !== '/') {
                    //Clean url
                    $except = trim($except, '/');
                }
                //If is exception route
                if ($request->is($except)) {
                    return $next($request);
                }
            }
            //Is not exception IP and not exception route launch 503

            return response()->view('errors.503');
        }
 
       return $next($request);
    }
}
