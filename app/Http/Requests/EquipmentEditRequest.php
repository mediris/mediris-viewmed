<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EquipmentEditRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $id = $this->id;

        return [
            'ae_title' => 'required|max:50|unique_test:equipment,' . $id,
            'name' => 'required|max:50',
            'room_id' => 'required',
            'modality_id' => 'required',
        ];
    }
}
