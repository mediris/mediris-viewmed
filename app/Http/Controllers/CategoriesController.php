<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Activity;
use App\Category;

class CategoriesController extends Controller {
    public function __construct() {
        $this->logPath = '/logs/admin/admin.log';
    }

    public function index( Request $request ){
        try{
            $categories = Category::remoteFindAll();

            return view('categories.index', compact('categories'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "categories", "index");
        }
    }

    public function add( Request $request ) {
        if( $request->isMethod('post') ){
            $this->validate($request, [ 'description' => 'required|max:45', ]);
            try {
                $data = $request->all();
                $data['language'] = \App::getLocale();

                $res = Category::remoteCreate( $data );

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-add'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('categories');
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "categories", "add");
            }
        }
        
        try {
            return view('categories.add');
        } catch( \Exception $e ) {
            return $this->logError( $e, "categories", "add");
        }
    }

    public function edit( Request $request, $id ){

        if( $request->isMethod('post') ){
            $this->validate($request, [ 'description' => 'required|max:45', ]);
            try{
                $data = $request->all();
                $data['language'] = \App::getLocale();

                $res = Category::remoteUpdate( $id, $data );

                if ( isset( $res->error ) ) {
                    return $this->parseFormErrors( $data, $res );
                } else {
                    $request->session()->flash('message', trans('alerts.success-edit'));
                    $request->session()->flash('class', 'alert alert-success');

                    return redirect()->route('categories');
                }
            } catch( \Exception $e ) {
                return $this->logError( $e, "categories", "edit");
            }
        }
        
        try {
            $category = Category::remoteFind( $id );

            return view('categories.edit', compact('category'));
        } catch( \Exception $e ) {
            return $this->logError( $e, "categories", "edit");
        }
    }

    public function active( Request $request, $id ){
        try{
            $res = Category::remoteToggleActive( $id );

            if ( isset( $res->error ) ) {
                return $this->parseFormErrors( $data, $res );
            } else {
                $request->session()->flash('message', trans('alerts.success-edit'));
                $request->session()->flash('class', 'alert alert-success');

                return redirect()->route('categories');
            }
        } catch( \Exception $e ) {
            return $this->logError( $e, "categories", "active");
        }
    }

}
