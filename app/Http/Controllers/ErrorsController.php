<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ErrorsController extends Controller {
    public function index( $id ){
        return view('errors.index', compact('id'));
    }

    public function forbidden(){
        return view('errors.403');
    }

    public function unauthorized(){
        return view('errors.401');
    }

    public function unavailable(){
        return view('errors.503');
    }
}
