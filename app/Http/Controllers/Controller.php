<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use Illuminate\Support\MessageBag;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getRoute( Request $request ){
        if( $request->isMethod('post') ){
            return route($request->all()['model'] . "." . $request->all()['actions'], [ $request->all()['id'] ]);
        }

        return "pascual";
    }

    function logError( $error, $section, $action ) {
    	Log::useFiles(	storage_path() . $this->logPath );
        Log::alert('Error code: ' . $error->getCode() . ' Error message: ' . $error->getMessage() . ' Section: ' . $section . '. Action: ' . $action);

        return view( 'errors.index',
        			[ 	'code' => $error->getCode(),
        				'message' => $error->getMessage(),
        				'file' => $error->getFile(),
        				'line' => $error->getLine(),
        				'trace' => $error->getTraceAsString()
        			]
        );
    }

    function parseFormErrors( $data, $response ) {
        $mb = new MessageBag();
        
        foreach( $data as $key => $value ) {
            if( strpos( strtoupper( $response->message ), strtoupper( $key ) ) !== false ) {
                if ( \Lang::has('errors.' . $response->error) ) {
                    $mb->add($key, trans('errors.' . $response->error, [ 'attribute' => trans('errors.attributes.' . $key) ]));
                }
            }
        }

        if( $mb->isEmpty() ) {
            return $this->parseError( $response );
        } else {
            return redirect()->back()->withErrors($mb)->withinput();
        }
    }

    function parseError( $response ) {
        $mb = new MessageBag();

        $mb->add( $response->error, $response->message );

        return redirect()->back()->withErrors($mb)->withinput();
    }
}