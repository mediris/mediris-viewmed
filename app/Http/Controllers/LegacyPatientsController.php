<?php

namespace App\Http\Controllers;

use Activity;
use App\Country;
use App\Http\Requests\PatientAddRequest;
use App\Oldrecordspaciente;
use App\PatientDocuments;
use App\Prefix;
use App\Responsable;
use App\ServiceRequest;
use App\Sex;
use App\Suffix;
use DataSourceResult;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Storage;
use Log;
use File;


class LegacyPatientsController extends Controller {

    public function __construct() {
        $this->logPath = '/logs/patients/legacypatients.log';

        //Constructor for managing kendo grid remote data binding
        $host = config('database.connections.mysql.host');
        $dbname = config('database.connections.mysql.database');
        $conn = config('database.default');
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');

        $this->DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);
    }

    /**
    * @fecha 25-11-2016
    * @programador Pascual Madrid / Juan Bigorra
    * @objetivo Renderiza la vista index de la sección Patients.
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
    */
    public function index( Request $request ) {
        try {
            if( $request->isMethod('POST') ) {
                $fields = [
                    'id',
                    'cedula',
                    'apellidos',
                    'nombres',
                    'email',
                    'telefonoFijo',
                    'telefonoMovil',
                ];

                $data = (object)$request->all();

                if( isset( $data->filter ) ){
                    $data->filter = (object)$data->filter;
                    foreach( $data->filter->filters as $key => $filter ){
                        $data->filter->filters[$key] = (object)$filter;
                    }
                }

                if( isset( $data->sort ) ){
                    foreach( $data->sort as $key => $sortArray ){
                        $data->sort[$key] = (object)$sortArray;
                    }
                }

                //$result = $this->DbHandler->read('patientsIndexView', $fields, $data);
                $result = $this->DbHandler->read('old_records_pacientes', $fields, $data);

                echo json_encode($result, JSON_UNESCAPED_UNICODE);
                exit();
            }

            return view('legacypatients.index');

        } catch( \Exception $e ) {
            return $this->logError( $e, "legacypatients", "index");
        }
    }

    public function history( Request $request, Oldrecordspaciente $patient ){
        try{
            return view('legacypatients.history', [ 'patient' => $patient ]);
        }catch( \Exception $e ){
            return $this->logError( $e, "patients", "history");
        }
    }
}
