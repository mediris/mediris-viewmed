<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class Category extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/categories';
        parent::__construct();
    }
}
