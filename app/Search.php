<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class Search extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/search';
        parent::__construct();
    }

    public static function remoteIndexData( $params, $action = null ) {

        $data = parent::remoteIndexData( $params );

        if( isset( $data->data ) ){
            foreach( $data->data as $key => $value ) {
                $value->orderStatus = ucfirst(trans('labels.' . $value->orderStatus));
                $value->patientSex = ucfirst(trans('labels.' . $value->patientSex));
            }
        }

        return $data;
    }
}
