<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Validation extends Model {
    public function __construct() {
        $this->client = new Client();
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
    }

    public function unique( $url, $table, $column, $value, $id = null ) {

        try {
            $data['api_token'] = \Auth::user()->api_token;
            $data['table'] = $table;
            $data['column'] = $column;
            $data['value'] = $value;

            if ( $id != null ) {
                $data['id'] = $id;
            }

            $res = $this->client->request('POST', $url . 'api/v1/unique', [ 'auth' => [ 'clientes', 'indev2015' ], 'headers' => $this->headers, 'form_params' => $data ]);

            if ( $res->getStatusCode() == 200 ) {
                return json_decode($res->getBody());
            }
        } catch ( \Exception $e ) {
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: validation. Action: add or edit');
            echo $e->getCode();
            echo '<br>';
            echo $e->getMessage();
        }
    }
}
