<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use App\Helpers\Utils;

class MedicalVisitor extends RemoteModel{

    use Notifiable;

    public function __construct( $attributes = []) {
        $this->apibase = 'api/v1/medicalvisitors';
        parent::__construct( $attributes );
    }

    /**
     * @fecha: 27-02-2024
     * @parametros: $url = Dirección del api de la institución donde se buscarán los datos, $id = Identificador de la
     *     instancia
     * @programador: Billy Gonzalez
     * @objetivo: Función para eliminar una instancia de Medical Visitor dado un identificador. NO ESTÁ EN USO
     */
    public function remove( $url, $id ) {

        $res = $this->client->request('POST',
            $url . $this->apibase . '/delete/' . $id, [
                'auth' => [ 'clientes', 'indev2015' ],
                'headers' => $this->headers,
                'form_params' => [
                    'api_token' => \Auth::user()->api_token,
                    'user_id' => \Auth::user()->id,
                ],
            ]);

        return $res;
    }

    public function user() {
        return User::find($this->user_id);
    }

    public function routeNotificationForMail() {
        return $this->email;
    }

    public function routeNotificationForMassivaMovil() {
        return Utils::formatCellphoneForMassivaMovil( $this->cellphone_number );
    }
}
