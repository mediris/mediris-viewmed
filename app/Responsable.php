<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsable extends Model {
    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'responsable_id', 'name',
    ];

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Responsable tiene muchos Patients.
     */
    public function patients() {
        return $this->hasMany(Patient::class);
    }

    /**
     * @fecha: 16-12-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
