<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use Session;
use Activity;

class RemoteModel extends Model
{
    public function __construct( $attributes = [] ) {
        $this->client = new Client();
        $this->headers = [ 'content-type' => 'application/x-www-form-urlencoded', 'X-Requested-With' => 'XMLHttpRequest' ];
        
        $this->createAction = '/add';
        $this->showAction   = '/show';
        $this->updateAction = '/edit';
        $this->indexAction  = '';
        $this->activeAction = '/active';

        $this->forceFill($attributes);
    }

    //BEGIN methods overriding
    //The following methods are required for the Notifications to work,
    //Because an instance can be created at any institution, then we transform the id into
    //id:id_intitutution so we can later find the instance from the database in the correct institution
    public function getKey() {
        return $this->getAttribute($this->getKeyName()) .":". $this->institution->id;
    }
    public function newQueryWithoutScopes() {return $this;}
    public function useWritePdo() {return $this;}

    public $valuesToGet = [];
    public function whereIn($column, $values, $boolean = 'and', $not = false) {
        foreach ($values as $value) {
            $this->valuesToGet[] = [$column, $value];
        }

        return $this;
    }
    public function get($columns = ['*']) {
        $id = $this->valuesToGet[0][1];
        return $this->findOrFail( $id );
    }
    public function findOrFail( $id, $columns = [] ) {
        $ids = explode( ":", $id );

        if (count($ids) == 2 ) {
            $institution = \App\Institution::find( $ids[1] );
            return $this->remoteFind( $ids[0], [], $institution );   
        } else {
            throw new \Exception("Bad id: " . $id . " " . get_class($this) );
        }
    }
    //END methods overriding

    //REQUESTS
     /**
     * @fecha: 15-06-2017
     * @parametros: $apiAction: action a ejecutarse en el API
                    $allInstitutions: Indica si se quiere el url para cada institución o solo el que indica la sesión de usuario
     * @retorno: Array con los urls para cada institution ya armado, si hay una sesión de usuario entonces solo devuelve
                el arreglo con el url de esa institución
     * @programador: Hendember Heras
     * @objetivo: Arma el url del endpoint para solo una o para todas las instituciones.
     */
    function buildEndpoint( $apiAction = "", $institution = null ) {
        if ( $institution ) {
            $this->institution = $institution;
        } elseif ( $this->institution == null ) {
            if ( Session::get('institution') ) {
                $this->institution = Session::get('institution');  
            } else {
                throw new \Exception( "No institution defined" );
            }
        }

        return $this->institution->url . $this->apibase . $apiAction;
    }
    
    function postRequest( $action, $data = array(), $institution = null ) {
        /*
        TO DO:
        El api siempre espera un api-token y un user_id, sin embargo este método se puede ejecutar en CLI
        por lo cual no hay usuario autenticado (no hay sesión), hay que permitir que el mediris
        tenga su propia api_token para estos casos o cambiar la forma de autenticación del mediris al API
        */
        /*
         * JCH: Se agrega la autenficacion para el guard('api'), la cual gestiona
        */
        if ( \Auth::user() ) {
            $data['api_token'] = \Auth::user()->api_token;  
            $data['user_id'] = \Auth::user()->id;
        } elseif ( \Auth::guard('api')->user() ) {
            $data['api_token'] = \Auth::guard('api')->user()->api_token;
            $data['user_id'] = \Auth::guard('api')->user()->id;
        } else {
            $data["api_token"] = "0LMLE6EPYKON6X5eXxgVtWBQ5P3ST6ku9mWM50U3kfs2cauqmP82luHtY9kA";
            $data["user_id"] = 1;
        }

        $endpoint = $this->buildEndpoint( $action, $institution );
        $response = $this->client->request('POST',
                                          	$endpoint,
                                            [
                                              'headers' => $this->headers, 
                                              'form_params' => $data,
                                              'http_errors' => false
                                            ]
        );

        if( $response->getStatusCode() == 200 ) {
            $responseJson = json_decode( $response->getBody() );

            if ( isset($responseJson->error) ) {
                //throw new \Exception($endpoint. " -> API error code:" . $responseJson->error . " API message: " . $responseJson->message);
                throw new \Exception($responseJson->message, $responseJson->error);
            } else {
                return $responseJson;
            }
        } else {
            throw new \Exception("HTTP message: " . $response->getReasonPhrase() .
                                " URL: " . $endpoint .
                                " BODY: " . (string)$response->getBody(), $response->getStatusCode());
        }
    }

    //LOGS
    //TO DO: remove parameter: data must get extracted from within the instance
    function logCreateApi( $data ) {
        $msg = trans('tracking.create-api',
                        [
                            'section' => get_class($this),
                            'id' => $data->id, 
                            'id-institution' => $this->institution->id
                       ]
                    );
        Activity::Log( $msg );
    }

    function logEditApi( $data ) {
        $msg = trans('tracking.edit-api',
                        [
                            'section' => get_class($this),
                            'id' => $data->oldValue->id,
                            'id-institution' => $this->institution->id,
                            'oldValue' => json_encode($data->oldValue),
                            'newValue' => json_encode($data->newValue)
                        ]
                    );

        Activity::Log( $msg );
    }

    //REMOTE METHODS
     /**
     * @fecha: 15-06-2017
     * @parametros: $id: id del objeto a buscar
     * @programador: Hendember Heras
     * @objetivo: Busca un objeto por el id en la base de datos remota del api.
     */
    public static function remoteFind( $id, $data = [], $institution = null ) {
        $model = new static();

        $response = $model->postRequest( $model->showAction . '/' . $id, $data, $institution ); 

        if ( $response ) {
            $model->forceFill( (array) $response );
            return $model;
        } else {
            return $response;
        }
    }
    /**
     * @fecha: 15-06-2017
     * @parametros: $data: arreglo para filtrar la búsqueda, por ejemplo $data['active'] = 1
     * @programador: Hendember Heras
     * @objetivo: Busca todas las instancias del modelo en la base de datos remota del api.
     */
    public static function remoteFindAll( $where = [], $institution = null ) {
        $model = new static();

        $where['where'] = $where;
        $response = $model->postRequest( $model->indexAction, $where, $institution ); 

        if ( $response ) {
            $elements = new Collection();
            foreach ( $response as $element ) {
                $tempmodel = new static();
                $tempmodel->forceFill( (array) $element );
                $elements->push( $tempmodel );
            }
            return $elements;
        } else {
            return collect($response);
        }
    }
     /**
     * @fecha: 15-06-2017
     * @parametros: $action: (string) ruta del api donde se muestra un listado de objetos (sirve para filtrar data, de esto se encarga el api),
                            por ejemplo "/recents", mostraría todos las instancias recientes (pero la implementación se haría del lado del api).
                    $params: (array) algún parámetro que espere el $action en el api. 
     * @programador: Hendember Heras
     * @objetivo: Busca objetos en un path específico del api, devolviendo un arreglo de objetos.
     */
    public static function remoteFindByAction( $action, $params = [], $institution = null ) {
        $model = new static();

        $response = $model->postRequest( $action, $params, $institution );

        if ( $response ) {

            $elements = new Collection();
            foreach ( $response as $element ) {
                $element->institution = $model->institution;
                $arrElement = (array) $element;
                $tempmodel = new static();
                $tempmodel->forceFill( $arrElement );
                $elements->push( $tempmodel );
            }
            
            return $elements;
        } else {
            return $response;
        }
    }
    /**
     * @fecha: 15-06-2017
     * @parametros: $attributes: arreglo de pares de key => values
     * @programador: Hendember Heras
     * @objetivo: Crea un objeto, a partir de los $attributes, devolviendo el objeto casteado a la clase correspondiente
                y grabandolo en la base de datos remota del api.
     */
    public static function remoteCreate( array $attributes = [] , $institution = null) {
        $model = new static();
        $response = $model->postRequest( $model->createAction, $attributes, $institution  );

        if ( isset( $response->newValue ) ) {
            $model->forceFill( (array) $response->newValue );
            $response->id = $response->newValue->id;
            $model->logCreateApi( $response );
            return $model;
        } else {
            return $response;
        }
    }
    /**
     * @fecha: 15-06-2017
     * @parametros: $id: id del objeto a editar
                    $attributes: arreglo de pares de key => values
     * @programador: Hendember Heras
     * @objetivo: Edita el objeto identificado por $id, con los valores de $attributes , devolviendo el objeto casteado a la clase correspondiente
                y grabandolo en la base de datos remota del api.
     */
    public static function remoteUpdate( $id, $attributes, $institution = null ) {
        $model = new static();
        
        $response = $model->postRequest( $model->updateAction . '/' . $id, $attributes, $institution );
        
        if ( isset( $response->newValue ) ) {
            if ( isset( $response->oldValue ) ) {
                $model->forceFill( (array) $response->oldValue );
                $model->syncOriginal();    
            }
            $model->forceFill( (array) $response->newValue );
            $model->logEditApi( $response );

            return $model;
        } else {
            return $response;
        }
    }
    /**
     * @fecha: 06-07-2017
     * @parametros: $id: id del objeto a activar/desactivar
     * @programador: Hendember Heras
     * @objetivo: Activa o desactiva el objeto identificado por $id, 
     * devolviendo el objeto casteado a la clase correspondiente y grabandolo en la base de datos remota del api.
     */
    public static function remoteToggleActive( $id, $institution = null ) {
        $model = new static();
        
        $response = $model->postRequest( $model->activeAction . '/' . $id, [], $institution );

        if ( isset( $response->newValue ) ) {
            $model->forceFill( (array) $response->newValue );
            $model->logEditApi( $response );

            return $model;
        } else {
            return $response;
        }
    }

    /**
     * @fecha: 21-08-2017
     * @parametros: $data
     * @programador: Hendember Heras
     * @objetivo: Devuelve el resultado del endpoint sin modificarlo o manipularlo, solo data, no se crean instancias
     */
    public static function remoteIndexData( $data, $action = null ) {
        $model = new static();

        if ( $action == null ) {
            $action = $model->indexAction; 
        }

        $response = $model->postRequest( $action, [ 'data' => $data ] );

        if ( isset( $response->data ) ) {
            return $response->data;
        }

        return $response;
    }
}
