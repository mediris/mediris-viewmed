<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

class PreAdmission extends RemoteModel {
    public function __construct( $attributes = []) {
        $this->apibase = 'api/v1/preadmission';
        parent::__construct( $attributes );
    }

    public static function indexRemoteData( $data, $institution = null ) {
        $model = new static();

        $params = ['data' => $data];
        $response = $model->postRequest( $model->indexAction, $params, $institution );

        if ( isset( $response->data ) ) {
            return $response->data;
        }

        return $response;
    }
}
