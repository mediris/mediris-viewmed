<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 06/09/17
 * Time: 04:31 PM
 */

namespace App;


class Modality2 extends RemoteModel {
    public function __construct() {
        $this->apibase = 'api/v1/modalities';
        parent::__construct();
    }
}