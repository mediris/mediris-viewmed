<?php

namespace App;
use App\Notifications\RequestedProcedureDone;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use URL;


class Fix extends RemoteModel{

    public function __construct() {
        $this->apibase = 'api/v1/fix';
        $this->externalApilogPath = '/logs/fix/apicallserrors.log';
        parent::__construct();
    }

    public static function remoteUpdate( $id, $data, $url = null ) {
        
    $model = new static();   
    $model = parent::remoteUpdate( $id, $data, $url);

        return json_encode($model) ;
    }
}