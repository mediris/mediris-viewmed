<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([ 'prefix' => 'preadmission' ], function () {
	Route::get('/', [ 'as' => 'preadmission', 'uses' => 'PreAdmissionControllerAPI@index' ])->middleware('auth:api');
	Route::post('/approve', [ 'as' => 'approve', 'uses' => 'PreAdmissionControllerAPI@approve' ])->middleware('auth:api');

    //Route::get('/', ['middleware' => 'auth:api', 'as' => 'preadmission', 'uses' => 'PreAdmissionControllerAPI@index']);
});
