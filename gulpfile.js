const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

	var sourcePath = 'resources/assets';
	var js_outputFile = 'public/js/app.js';
	var sass_outputFile = '/css/logoscorp.css';
	var sass_printOutputFile = '/css/print.css';
	var css_outputFile = 'public/css/app.css';

	mix
		/**
		 * CSS files compilation to public/css folder
		 */
		.styles([
				'css/style.min.css',
				'css/style.css',
				'bower_components/bootstrap/dist/css/bootstrap.min.css',
				'bower_components/font-awesome/css/font-awesome.min.css',
				'bower_components/bootstrap-select/dist/css/bootstrap-select.min.css',
				'bower_components/jquery-colorbox/example1/colorbox.css',
				'bower_components/cropper/dist/cropper.css'

				//'plugins/croppic/assets/css/croppic.css'
			],
			'public/css/app1.min.css',
			sourcePath
		)

		.sass([
			'../plugins/croppic/assets/css/croppic.css',
			'logoscorp.scss',
			'../css/pace.css'
		], 'public/css/app2.min.css' )


		.sass([
				'../css/bootstrap-print.css',
				'print.scss'
			],
			'public/css/print.min.css'
		)

		.sass('pdfs.scss')

		.version(["css/app2.min.css", "css/print.min.css"])

		/**
		 * JavaScript files compilation to app.js
		 */

		.scripts([
				'bower_components/jquery/dist/jquery.min.js',
				'bower_components/bootstrap/dist/js/bootstrap.min.js',
				'bower_components/jquery-colorbox/jquery.colorbox-min.js',
				'bower_components/moment/min/moment.min.js',
				'plugins/croppic/croppic.js',
				'js/pace.js',
				'js/jszip.min.js',
				'js/pako.min.js',
			],
			'public/js/app1.min.js',
			sourcePath
		)

		.scripts([
				'plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js',
				'bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
				'plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js',
				'js/common.js',
				'js/logoscorp.js',
				'js/scheduler.js',
				'js/webcam.js',
				'js/modernizr-custom.js',
				'bower_components/cropper/dist/cropper.js',
			],
			'public/js/app2.min.js',
			sourcePath
		)

		.scripts([
				'js/checkMessages.js'
			],
			'public/js/checkMessages.min.js',
			sourcePath
		)

		/**
		 * Copy fonts to build folder
		 */
		.copy(sourcePath + '/fonts', 'public/fonts')
		.copy(sourcePath + '/bower_components/bootstrap/fonts', 'public/fonts')
		.copy(sourcePath + '/bower_components/font-awesome/fonts', 'public/fonts')
		.copy(sourcePath + '/plugins/croppic/assets/img', 'public/build/img')
		.copy(sourcePath + '/images', 'public/images');

});